import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {HomeResolverService} from '../service/home-resolver.service';
import {AuthorizationService} from '../service/authorization.service';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
  constructor(
        private homeResolverService : HomeResolverService,
        private authorizationService : AuthorizationService,
        private router: Router,
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const requestWithToken = this.appendTokenToHeader(req);
    return next.handle(requestWithToken).pipe(
        tap(
            () => { },
            (error) => {
              if (error instanceof HttpErrorResponse && error.status === 401) {
                this.authorizationService.logout();
                this.router.navigate([this.homeResolverService.userHome()]);
              }
            },
        ),
    );
  }

  appendTokenToHeader(req: HttpRequest<any>): HttpRequest<any> {
    const token = this.authorizationService.authenticationToken;

    if (token) {
      const authReq = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${token}`),
      });
      return authReq;
    } else {
      return req;
    }
  }
}
