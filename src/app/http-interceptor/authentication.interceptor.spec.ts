import {HttpClient, HTTP_INTERCEPTORS} from '@angular/common/http';
import {TestBed} from '@angular/core/testing';
import {Router} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {HomeResolverService} from '../service/home-resolver.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import {AuthenticationInterceptor} from './authentication.interceptor';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {AuthorizationService} from '../service/authorization.service';

const mockUrl = 'http://localhost:8080/';
const mockParamer = 'parameter';

@Injectable({
  providedIn: 'root',
})
class StubHttpService {
  constructor(private httpClient: HttpClient) {
  }

  mockRequest(): Observable<void> {
    return this.httpClient.post<void>(mockUrl, mockParamer);
  }
}

describe('AuthenticationInterceptor', () => {
  let routerSpy: jasmine.SpyObj<Router>;
  let authorizationServiceSpy: jasmine.SpyObj<AuthorizationService>;
  let homeResolverSpy: jasmine.SpyObj<HomeResolverService>;
  let httpMock: HttpTestingController;
  let stubHttpService: StubHttpService;

  beforeEach(() => {
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    authorizationServiceSpy = jasmine.createSpyObj<AuthorizationService>('AuthorizationService', ['logout', 'authenticationToken']);
    homeResolverSpy = jasmine.createSpyObj<HomeResolverService>('HomeResolverService', ['userHome']);

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule, HttpClientTestingModule,
      ],
      providers: [
        StubHttpService,
        {provide: AuthorizationService, useValue: authorizationServiceSpy},
        AuthenticationInterceptor,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthenticationInterceptor,
          multi: true,
        },
        {provide: HomeResolverService, useValue: homeResolverSpy},
        {provide: Router, useValue: routerSpy},
        HttpClient,
      ],
    });

    httpMock = TestBed.inject(HttpTestingController);
    localStorage.clear();
    stubHttpService = TestBed.inject(StubHttpService);
  });

  describe('Append header on request', () => {
    it('with access token', () => {
      const accessToken = 'AnAccessToken';
      authorizationServiceSpy.authenticationToken = accessToken;
      stubHttpService.mockRequest().subscribe(() => { });

      const httpRequest = httpMock.expectOne(mockUrl);
      httpRequest.flush({status: 200});
      expect(httpRequest.request.headers.has('Authorization')).toBeTrue();
      expect(httpRequest.request.headers.get('Authorization')).toEqual('Bearer ' + accessToken);
    });
    it('without access token', () => {
      authorizationServiceSpy.authenticationToken = null;
      stubHttpService.mockRequest().subscribe(() => { });

      const httpRequest = httpMock.expectOne(mockUrl);
      httpRequest.flush({status: 200});
      expect(httpRequest.request.headers.has('Authorization')).toBeFalse();
    });
  });
  describe('Intercept 401 unauthorized', () => {
    it('On 401 error', () => {
      const userHome = of('home');
      homeResolverSpy.userHome.and.returnValue(userHome);

      stubHttpService.mockRequest().subscribe(() => {
        fail();
      }, () => { });

      httpMock.expectOne(mockUrl).flush('Unauthorized error', {status: 401, statusText: 'Unauthorized'});
      expect(authorizationServiceSpy.logout).toHaveBeenCalledTimes(1);

      expect(homeResolverSpy.userHome).toHaveBeenCalledTimes(1);
      expect(routerSpy.navigate).toHaveBeenCalledOnceWith([userHome]);
    });
    it('On other error', () => {
      const userHome = of('home');
      homeResolverSpy.userHome.and.returnValue(userHome);

      stubHttpService.mockRequest().subscribe(() => {
        fail();
      }, () => { });

      httpMock.expectOne(mockUrl).flush('Internal error', {status: 500, statusText: 'Internal error'});
      expect(authorizationServiceSpy.logout).toHaveBeenCalledTimes(0);

      expect(homeResolverSpy.userHome).toHaveBeenCalledTimes(0);
      expect(routerSpy.navigate).toHaveBeenCalledTimes(0);
    });
    it('No error', () => {
      const userHome = of('home');
      homeResolverSpy.userHome.and.returnValue(userHome);

      stubHttpService.mockRequest().subscribe(() => { });

      httpMock.expectOne(mockUrl).flush( {status: 200});
      expect(authorizationServiceSpy.logout).toHaveBeenCalledTimes(0);

      expect(homeResolverSpy.userHome).toHaveBeenCalledTimes(0);
      expect(routerSpy.navigate).toHaveBeenCalledTimes(0);
    });
  });

  afterEach(() => {
    localStorage.clear();
  });
});
