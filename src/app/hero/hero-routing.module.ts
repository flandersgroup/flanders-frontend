import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthorizedGuard} from '../guard/authorized.guard';
import {Authorizations} from 'common/model/authorizations';
import {MyMissionComponent} from './my-mission/my-mission.component';
import {SubmitMissionComponent} from './submit-mission/submit-mission.component';

const routes: Routes = [
  {
    path: 'mymissions', component: MyMissionComponent,
    canActivate: [AuthorizedGuard],
    data: {authorizations: [Authorizations.CanSeeHisSubmittedMissions]},
  },
  {
    path: 'submitmission', component: SubmitMissionComponent,
    canActivate: [AuthorizedGuard],
    data: {authorizations: [Authorizations.CanSubmitMission]},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HeroRoutingModule { }
