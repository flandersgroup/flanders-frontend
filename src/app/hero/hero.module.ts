import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HeroRoutingModule} from './hero-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MyMissionComponent} from './my-mission/my-mission.component';
import {SubmitMissionComponent} from './submit-mission/submit-mission.component';

@NgModule({
  declarations: [MyMissionComponent, SubmitMissionComponent],
  imports: [
    CommonModule,
    HeroRoutingModule,
    NgbModule,
  ],
})
export class HeroModule { }
