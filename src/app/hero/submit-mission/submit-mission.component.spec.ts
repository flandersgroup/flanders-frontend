import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SubmitMissionComponent} from './submit-mission.component';

describe('SubmitMissionComponent', () => {
  let component: SubmitMissionComponent;
  let fixture: ComponentFixture<SubmitMissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SubmitMissionComponent],
    })
        .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitMissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
