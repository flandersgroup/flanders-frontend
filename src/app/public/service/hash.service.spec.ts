import {TestBed} from '@angular/core/testing';

import {HashService} from './hash.service';
import * as bcrypt from 'bcryptjs';

describe('HashService', () => {
  let service: HashService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HashService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('hash', async () => {
    const toHash = 'wordToHash';
    expect(await service.hash(toHash)).toEqual(await bcrypt.hash(toHash, service.hashSalt));
  });
});
