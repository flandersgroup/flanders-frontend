import {Injectable} from '@angular/core';

import * as bcrypt from 'bcryptjs';

@Injectable({
  providedIn: 'root',
})
export class HashService {
  private _hashSalt: string = '$2a$10$XbMt.meoZlp4zLN2PgvJDO';

  constructor() { }

  hash(value: string): Promise<string> {
    return bcrypt.hash(value, this._hashSalt);
  }

  get hashSalt() : string {
    return this._hashSalt;
  }
}
