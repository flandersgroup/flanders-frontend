import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AuthenticationService} from 'src/app/service/authentication.service';
import {HttpClient} from '@angular/common/http';
import {AuthenticationParameters} from 'common/server/parameter/authentication';
import {LoginResponse} from 'common/server/response/login';
import {environment} from 'src/environments/environment';
import {ProfileResponse} from 'common/server/response/profile';
import {RegistrationParameters} from 'common/server/parameter/registration';
import {RegisteredParameters} from 'common/server/parameter/registered';
import {ResetPasswordRequestParameters} from 'common/server/parameter/resetPasswordRequest';
import {ResetPasswordParameters} from 'common/server/parameter/resetPassword';
import {HashService} from './hash.service';


@Injectable({
  providedIn: 'root',
})
export class LoginService {
  private loginResponse: Observable<LoginResponse> | undefined = undefined;
  private registrableProfilesResponse : Observable<ProfileResponse[]> | undefined = undefined;
  private registerResponse: Observable<void> | undefined = undefined;
  private registeredResponse: Observable<LoginResponse> | undefined = undefined;
  private resetPasswordRequestResponse: Observable<void> | undefined = undefined;
  private resetPasswordResponse: Observable<void> | undefined = undefined;

  constructor(private httpClient: HttpClient, private authenticationService: AuthenticationService, private hashService: HashService) {
  }

  login(email: string, password: string): Observable<LoginResponse> {
    if (this.loginResponse) {
      return this.loginResponse;
    }

    const loginResponse = new Subject<LoginResponse>();
    this.loginResponse = loginResponse;

    const url = environment.apiUrl + '/login';
    this.hashService.hash(password).then((hashedPassword) => {
      const authenticationParameter = new AuthenticationParameters();
      authenticationParameter.email = email;
      authenticationParameter.password = hashedPassword;

      const res = this.httpClient.post<LoginResponse>(url, authenticationParameter);

      res.subscribe((result) => {
        this.authenticationService.login(result.token);
        this.loginResponse = undefined;
        loginResponse.next(result);
      }, (error) => {
        this.loginResponse = undefined;
        loginResponse.error(error);
      });
    }).catch((error) => {
      this.loginResponse = undefined;
      loginResponse.error(error);
    });

    return loginResponse;
  }


  registrableProfiles(): Observable<ProfileResponse[]> {
    if (this.registrableProfilesResponse) {
      return this.registrableProfilesResponse;
    }

    const registrableProfilesResponse = new Subject<ProfileResponse[]>();
    this.registrableProfilesResponse = registrableProfilesResponse;

    const url = environment.apiUrl + '/registrableProfiles';
    this.httpClient.get<ProfileResponse[]>(url).subscribe((response) => {
      this.registrableProfilesResponse = undefined;
      registrableProfilesResponse.next(response);
    }, (error) => {
      this.registrableProfilesResponse = undefined;
      registrableProfilesResponse.error(error);
    });

    return registrableProfilesResponse;
  }

  register(registrationParameters: RegistrationParameters): Observable<void> {
    if (this.registerResponse) {
      return this.registerResponse;
    }

    const registerResponse = new Subject<void>();
    this.registerResponse = registerResponse;

    this.hashService.hash(registrationParameters.password).then((hashedPassword) => {
      registrationParameters.password = hashedPassword;
      const url = environment.apiUrl + '/register';
      this.httpClient.post<void>(url, registrationParameters).subscribe(() => {
        this.registerResponse = undefined;
        registerResponse.next();
      }, (error) => {
        this.registerResponse = undefined;
        registerResponse.error(error);
      });
    }).catch((error) => {
      this.registerResponse = undefined;
      registerResponse.error(error);
    });

    return registerResponse;
  }

  registered(registeredParameters: RegisteredParameters): Observable<LoginResponse> {
    if (this.registeredResponse) {
      return this.registeredResponse;
    }

    const registeredResponse = new Subject<LoginResponse>();
    this.registeredResponse = registeredResponse;
    const url = environment.apiUrl + '/registered';
    this.httpClient.post<LoginResponse>(url, registeredParameters).subscribe((result) => {
      this.authenticationService.login(result.token);
      registeredResponse.next(result);
      this.registeredResponse = undefined;
    }, (error) => {
      this.authenticationService.logout();
      registeredResponse.error(error);
      this.registeredResponse = undefined;
    });
    return registeredResponse;
  }

  resetPasswordRequest(resetPasswordRequestParameters: ResetPasswordRequestParameters): Observable<void> {
    if (this.resetPasswordRequestResponse) {
      return this.resetPasswordRequestResponse;
    }

    const resetPasswordRequestResponse = new Subject<void>();
    this.resetPasswordRequestResponse = resetPasswordRequestResponse;

    const url = environment.apiUrl + '/resetPasswordRequest';
    this.httpClient.post<void>(url, resetPasswordRequestParameters).subscribe(() => {
      this.resetPasswordRequestResponse = undefined;
      resetPasswordRequestResponse.next();
    }, (error)=> {
      this.resetPasswordRequestResponse = undefined;
      resetPasswordRequestResponse.error(error);
    });

    return resetPasswordRequestResponse;
  }

  resetPassword(resetPasswordParameters: ResetPasswordParameters): Observable<void> {
    if (this.resetPasswordResponse) {
      return this.resetPasswordResponse;
    }
    const resetPasswordReponse = new Subject<void>();
    this.resetPasswordResponse = resetPasswordReponse;

    this.hashService.hash(resetPasswordParameters.password).then((hashedPassword) => {
      resetPasswordParameters.password = hashedPassword;
      const url = environment.apiUrl + '/resetPassword';
      this.httpClient.post<void>(url, resetPasswordParameters).subscribe(() => {
        this.resetPasswordResponse = undefined;
        resetPasswordReponse.next();
      }, (error) => {
        this.resetPasswordResponse = undefined;
        resetPasswordReponse.error(error);
      });
    }).catch((error) => {
      this.resetPasswordResponse = undefined;
      resetPasswordReponse.error(error);
    });
    return this.resetPasswordResponse;
  }
}
