import {HttpBackend, HttpClient} from '@angular/common/http';
import {TestBed} from '@angular/core/testing';
import {asapScheduler, scheduled, Subject, throwError} from 'rxjs';
import {AuthenticationParameters} from 'common/server/parameter/authentication';
import {LoginResponse} from 'common/server/response/login';
import {AuthenticationService} from 'src/app/service/authentication.service';
import {environment} from 'src/environments/environment';

import {LoginService} from './login.service';
import {RegisteredParameters} from 'common/server/parameter/registered';
import {RegistrationParameters} from 'common/server/parameter/registration';
import {ProfileResponse} from 'common/server/response/profile';
import {ResetPasswordRequestParameters} from 'common/server/parameter/resetPasswordRequest';
import {ResetPasswordParameters} from 'common/server/parameter/resetPassword';
import {HashService} from './hash.service';

describe('LoginService', () => {
  let service: LoginService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let authenticationServiceSpy: jasmine.SpyObj<AuthenticationService>;
  let hashServiceSpy: jasmine.SpyObj<HashService>;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj<HttpClient>('HttpClient', ['get', 'post']);
    authenticationServiceSpy = jasmine.createSpyObj<AuthenticationService>('AuthenticationService', ['login', 'logout']);
    hashServiceSpy = jasmine.createSpyObj<HashService>('HashService', ['hash']);

    TestBed.configureTestingModule({
      providers: [
        LoginService,
        HttpBackend,
        {provide: HttpClient, useValue: httpClientSpy},
        {provide: AuthenticationService, useValue: authenticationServiceSpy},
        {provide: HashService, useValue: hashServiceSpy},
      ],
    });

    service = TestBed.inject(LoginService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('login', () => {
    it('login succeed', (done) => {
      const mockResponse = new LoginResponse();
      mockResponse.token = 'faketoken';

      const password = 'password';
      const hashPassword = 'hashPassword';

      httpClientSpy.post.and.returnValue(scheduled([mockResponse], asapScheduler));
      hashServiceSpy.hash.and.resolveTo(hashPassword);

      const authenticationParameter = new AuthenticationParameters();
      authenticationParameter.email = 'email';
      authenticationParameter.password = hashPassword;

      service.login(authenticationParameter.email, password).subscribe((response) => {
        expect(response).toEqual(mockResponse);

        expect(authenticationServiceSpy.login).toHaveBeenCalledOnceWith(mockResponse.token);
        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/login', authenticationParameter);
        expect(hashServiceSpy.hash).toHaveBeenCalledOnceWith(password);
        done();
      }, fail);
    });

    it('hash error', (done) => {
      const password = 'password';

      hashServiceSpy.hash.and.rejectWith(new Error('Hash error'));

      const authenticationParameter = new AuthenticationParameters();
      authenticationParameter.email = 'email';

      service.login(authenticationParameter.email, password).subscribe(fail, (error) => {
        expect(error).toBeInstanceOf(Error);
        expect((error as Error).message).toEqual('Hash error');

        expect(authenticationServiceSpy.login).toHaveBeenCalledTimes(0);
        expect(httpClientSpy.post).toHaveBeenCalledTimes(0);
        expect(hashServiceSpy.hash).toHaveBeenCalledOnceWith(password);
        done();
      });
    });

    it('login failed', (done) => {
      const password = 'password';
      const hashPassword = 'hashPassword';

      const authenticationParameter = new AuthenticationParameters();
      authenticationParameter.email = 'email';
      authenticationParameter.password = hashPassword;

      httpClientSpy.post.and.returnValue(throwError({status: 404}));
      hashServiceSpy.hash.and.resolveTo(hashPassword);

      service.login(authenticationParameter.email, password).subscribe(fail, (error) => {
        expect(error.status).toEqual(404);
        expect(authenticationServiceSpy.login).toHaveBeenCalledTimes(0);
        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/login', authenticationParameter);
        expect(hashServiceSpy.hash).toHaveBeenCalledOnceWith(password);
        done();
      });
    });

    it('login failed then succeed', (done) => {
      const password = 'password';
      const hashPassword = 'hashPassword';

      const authenticationParameter = new AuthenticationParameters();
      authenticationParameter.email = 'email';
      authenticationParameter.password = hashPassword;

      httpClientSpy.post.and.returnValue(throwError({status: 404}));
      hashServiceSpy.hash.and.resolveTo(hashPassword);

      service.login(authenticationParameter.email, password).subscribe(fail, (error) => {
        expect(error.status).toEqual(404);
        expect(authenticationServiceSpy.login).toHaveBeenCalledTimes(0);
        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/login', authenticationParameter);
        expect(hashServiceSpy.hash).toHaveBeenCalledOnceWith(password);

        const mockResponse = new LoginResponse();
        mockResponse.token = 'faketoken';

        httpClientSpy.post.and.returnValue(scheduled([mockResponse], asapScheduler));

        service.login(authenticationParameter.email, password).subscribe((response) => {
          expect(response).toEqual(mockResponse);

          expect(authenticationServiceSpy.login).toHaveBeenCalledOnceWith(mockResponse.token);
          expect(httpClientSpy.post).toHaveBeenCalledTimes(2);
          expect(httpClientSpy.post).toHaveBeenCalledWith(environment.apiUrl + '/login', authenticationParameter);
          expect(hashServiceSpy.hash).toHaveBeenCalledTimes(2);
          expect(hashServiceSpy.hash).toHaveBeenCalledWith(password);
          done();
        });
      });
    });

    it('multiple request', (done) => {
      const mockResponse = new LoginResponse();
      mockResponse.token = 'faketoken';

      const password = 'password';
      const hashPassword = 'hashPassword';

      hashServiceSpy.hash.and.resolveTo(hashPassword);

      const authenticationParameter = new AuthenticationParameters();
      authenticationParameter.email = 'email';
      authenticationParameter.password = hashPassword;

      const resultObservable = service.login(authenticationParameter.email, password);
      resultObservable.subscribe((response) => {
        expect(response).toEqual(mockResponse);

        expect(authenticationServiceSpy.login).toHaveBeenCalledOnceWith(mockResponse.token);
        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/login', authenticationParameter);
        expect(hashServiceSpy.hash).toHaveBeenCalledOnceWith(password);
        done();
      }, fail);

      expect(service.login(authenticationParameter.email, password)).toEqual(resultObservable);
      expect(service.login(authenticationParameter.email, password)).toEqual(resultObservable);

      httpClientSpy.post.and.returnValue(scheduled([mockResponse], asapScheduler));
    });
  });

  describe('registrableProfiles', () => {
    it('success', (done) => {
      const profiles: ProfileResponse[] = [{name: 'Profile1', description: 'test1'},
        {name: 'Profile2', description: 'test2'}, {name: 'Profile3', description: 'test3'}];

      const returnObservable = new Subject<ProfileResponse[]>();

      httpClientSpy.get.and.returnValue(returnObservable);

      service.registrableProfiles().subscribe((response) => {
        expect(response).toEqual(profiles);

        expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
        done();
      }, fail);

      returnObservable.next(profiles);
    });

    it('fail', (done) => {
      httpClientSpy.get.and.returnValue(throwError({status: 404}));

      service.registrableProfiles().subscribe(fail, (error) => {
        expect(error.status).toEqual(404);

        expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
        done();
      }, fail);
    });

    it('multiple requests', (done) => {
      const profiles: ProfileResponse[] = [{name: 'Profile1', description: 'test1'},
        {name: 'Profile2', description: 'test2'}, {name: 'Profile3', description: 'test3'}];

      const returnObservable = new Subject<ProfileResponse[]>();

      httpClientSpy.get.and.returnValue(returnObservable);

      const resultObservable = service.registrableProfiles();

      resultObservable.subscribe((response) => {
        expect(response).toEqual(profiles);

        expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
        done();
      }, fail);

      expect(service.registrableProfiles()).toEqual(resultObservable);
      expect(service.registrableProfiles()).toEqual(resultObservable);

      returnObservable.next(profiles);
    });
  });

  describe('register', () => {
    it('success', (done) => {
      const password = 'password';
      const hashedPassword = 'hashedPassword';

      const registrationParameters = new RegistrationParameters();
      registrationParameters.email = 'email';
      registrationParameters.password = password;
      registrationParameters.profile = 'aProfile';

      httpClientSpy.post.and.returnValue(scheduled(['any'], asapScheduler));
      hashServiceSpy.hash.and.resolveTo(hashedPassword);

      service.register(registrationParameters).subscribe(() => {
        expect(hashServiceSpy.hash).toHaveBeenCalledOnceWith(password);
        registrationParameters.password = hashedPassword;

        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/register', registrationParameters);
        done();
      }, fail);
    });

    it('hash error', (done) => {
      const registrationParameters = new RegistrationParameters();
      registrationParameters.email = 'email';
      registrationParameters.password = 'password';
      registrationParameters.profile = 'aProfile';

      hashServiceSpy.hash.and.rejectWith(new Error('Hash error'));

      service.register(registrationParameters).subscribe(fail, () => {
        expect(hashServiceSpy.hash).toHaveBeenCalledOnceWith(registrationParameters.password);
        done();
      });
    });

    it('register request error', (done) => {
      const password = 'password';
      const hashedPassword = 'hashedPassword';

      const registrationParameters = new RegistrationParameters();
      registrationParameters.email = 'email';
      registrationParameters.password = password;
      registrationParameters.profile = 'aProfile';

      httpClientSpy.post.and.returnValue(throwError({status: 404}));
      hashServiceSpy.hash.and.resolveTo(hashedPassword);

      service.register(registrationParameters).subscribe(fail, (error) => {
        expect(error.status).toEqual(404);
        expect(hashServiceSpy.hash).toHaveBeenCalledOnceWith(password);

        registrationParameters.password = hashedPassword;

        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/register', registrationParameters);
        done();
      });
    });

    it('multiple request', (done) => {
      const password = 'password';
      const hashedPassword = 'hashedPassword';

      const registrationParameters = new RegistrationParameters();
      registrationParameters.email = 'email';
      registrationParameters.password = password;
      registrationParameters.profile = 'aProfile';

      hashServiceSpy.hash.and.resolveTo(hashedPassword);

      const resultObservable = service.register(registrationParameters);
      resultObservable.subscribe(() => {
        expect(hashServiceSpy.hash).toHaveBeenCalledOnceWith(password);
        registrationParameters.password = hashedPassword;

        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/register', registrationParameters);
        done();
      }, fail);

      expect(service.register(registrationParameters)).toEqual(resultObservable);
      expect(service.register(registrationParameters)).toEqual(resultObservable);

      httpClientSpy.post.and.returnValue(scheduled(['any'], asapScheduler));
    });
  });

  describe('registered', () => {
    it('registered succeed', (done) => {
      const mockResponse = new LoginResponse();
      mockResponse.token = 'faketoken';

      const returnObservable = new Subject<LoginResponse>();

      httpClientSpy.post.and.returnValue(returnObservable);

      const registeredParameters = new RegisteredParameters;
      registeredParameters.tokenData = 'mockTokenData';

      service.registered(registeredParameters).subscribe((response) => {
        expect(response).toEqual(mockResponse);

        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/registered', registeredParameters);

        expect(authenticationServiceSpy.login).toHaveBeenCalledOnceWith(mockResponse.token);
        done();
      }, fail);

      returnObservable.next(mockResponse);
    });

    it('registered failed', (done) => {
      const mockResponse = new LoginResponse();
      mockResponse.token = 'faketoken';

      httpClientSpy.post.and.returnValue(throwError({status: 401}));

      const registeredParameters = new RegisteredParameters;
      registeredParameters.tokenData = 'mockTokenData';

      service.registered(registeredParameters).subscribe(fail, (error) => {
        expect(error.status).toEqual(401);
        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/registered', registeredParameters);
        expect(authenticationServiceSpy.logout).toHaveBeenCalledOnceWith();

        done();
      }, fail);
    });

    it('multiple request', (done) => {
      const mockResponse = new LoginResponse();
      mockResponse.token = 'faketoken';

      const returnObservable = new Subject<LoginResponse>();

      httpClientSpy.post.and.returnValue(returnObservable);

      const registeredParameters = new RegisteredParameters;
      registeredParameters.tokenData = 'mockTokenData';

      const resultObservable = service.registered(registeredParameters);

      resultObservable.subscribe((response) => {
        expect(response).toEqual(mockResponse);

        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/registered', registeredParameters);

        expect(authenticationServiceSpy.login).toHaveBeenCalledOnceWith(mockResponse.token);
        done();
      }, fail);

      expect(service.registered(registeredParameters)).toEqual(resultObservable);
      expect(service.registered(registeredParameters)).toEqual(resultObservable);

      returnObservable.next(mockResponse);
    });
  });

  describe('resetPasswordRequest', () => {
    it('success', (done) => {
      const resetPasswordRequestParameters = new ResetPasswordRequestParameters();
      resetPasswordRequestParameters.email = 'email';

      httpClientSpy.post.and.returnValue(scheduled(['any'], asapScheduler));

      service.resetPasswordRequest(resetPasswordRequestParameters).subscribe(() => {
        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/resetPasswordRequest', resetPasswordRequestParameters);
        done();
      }, fail);
    });
    it('fail', (done) => {
      const resetPasswordRequestParameters = new ResetPasswordRequestParameters();
      resetPasswordRequestParameters.email = 'email';

      httpClientSpy.post.and.returnValue(throwError({status: 401}));

      service.resetPasswordRequest(resetPasswordRequestParameters).subscribe(fail, (error) => {
        expect(error.status).toEqual(401);
        done();
      });
    });
    it('multiple requests', (done) => {
      const resetPasswordRequestParameters = new ResetPasswordRequestParameters();
      resetPasswordRequestParameters.email = 'email';

      const returnObservable = new Subject<void>();

      httpClientSpy.post.and.returnValue(returnObservable);
      const resultObservable = service.resetPasswordRequest(resetPasswordRequestParameters);

      resultObservable.subscribe(() => {
        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/resetPasswordRequest', resetPasswordRequestParameters);
        done();
      }, fail);

      expect(service.resetPasswordRequest(resetPasswordRequestParameters)).toEqual(resultObservable);
      expect(service.resetPasswordRequest(resetPasswordRequestParameters)).toEqual(resultObservable);

      returnObservable.next();
    });
  });

  describe('resetPassword', () => {
    it('success', (done) => {
      const password = 'password';
      const hashedPassword = 'hashedPassword';

      const resetPasswordParameters = new ResetPasswordParameters();
      resetPasswordParameters.tokenData = 'SomeTokenData';
      resetPasswordParameters.password = password;

      httpClientSpy.post.and.returnValue(scheduled(['any'], asapScheduler));
      hashServiceSpy.hash.and.resolveTo(hashedPassword);

      service.resetPassword(resetPasswordParameters).subscribe(() => {
        expect(hashServiceSpy.hash).toHaveBeenCalledOnceWith(password);
        resetPasswordParameters.password = hashedPassword;
        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/resetPassword', resetPasswordParameters);
        done();
      }, fail);
    });

    it('hash fail', (done) => {
      const password = 'ANewPassword';
      const hashedPassword = 'hashedPassword';

      const resetPasswordParameters = new ResetPasswordParameters();
      resetPasswordParameters.tokenData = 'SomeTokenData';
      resetPasswordParameters.password = password;

      httpClientSpy.post.and.returnValue(scheduled(['any'], asapScheduler));
      hashServiceSpy.hash.and.rejectWith(new Error('Hash failed'));

      service.resetPassword(resetPasswordParameters).subscribe(fail, (error) => {
        expect(error.message).toEqual('Hash failed');
        expect(hashServiceSpy.hash).toHaveBeenCalledOnceWith(password);
        resetPasswordParameters.password = hashedPassword;
        expect(httpClientSpy.post).toHaveBeenCalledTimes(0);
        done();
      });
    });

    it('request fail', (done) => {
      const password = 'password';
      const hashedPassword = 'hashedPassword';

      const resetPasswordParameters = new ResetPasswordParameters();
      resetPasswordParameters.tokenData = 'SomeTokenData';
      resetPasswordParameters.password = password;

      httpClientSpy.post.and.returnValue(throwError({status: 401}));
      hashServiceSpy.hash.and.resolveTo(hashedPassword);

      service.resetPassword(resetPasswordParameters).subscribe(fail, (error) => {
        expect(error.status).toEqual(401);
        expect(hashServiceSpy.hash).toHaveBeenCalledOnceWith(password);
        resetPasswordParameters.password = hashedPassword;
        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/resetPassword', resetPasswordParameters);
        done();
      });
    });

    it('multiple requests', (done) => {
      const password = 'password';
      const hashedPassword = 'hashedPassword';

      const resetPasswordParameters = new ResetPasswordParameters();
      resetPasswordParameters.tokenData = 'SomeTokenData';
      resetPasswordParameters.password = password;

      hashServiceSpy.hash.and.resolveTo(hashedPassword);

      const resultObservable = service.resetPassword(resetPasswordParameters);

      resultObservable.subscribe(() => {
        expect(hashServiceSpy.hash).toHaveBeenCalledOnceWith(password);
        resetPasswordParameters.password = hashedPassword;
        expect(httpClientSpy.post).toHaveBeenCalledOnceWith(environment.apiUrl + '/resetPassword', resetPasswordParameters);
        done();
      }, fail);

      expect(service.resetPassword(resetPasswordParameters));

      httpClientSpy.post.and.returnValue(scheduled(['any'], asapScheduler));
    });
  });
});
