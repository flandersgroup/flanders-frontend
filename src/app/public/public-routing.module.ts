import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {RegisteredComponent} from './registered/registered.component';
import {RegistrationComponent} from './registration/registration.component';
import {ResetPasswordRequestComponent} from './reset-password-request/reset-password-request.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegistrationComponent},
  {path: 'registered', component: RegisteredComponent},
  {path: 'resetPasswordRequest', component: ResetPasswordRequestComponent},
  {path: 'resetPassword', component: ResetPasswordComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicRoutingModule { }
