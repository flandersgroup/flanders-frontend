import {Component, OnInit} from '@angular/core';

import * as AOS from 'aos';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})


export class HomeComponent implements OnInit {
  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);

  // eslint-disable-next-line max-len
  vilains: VilainProfile[] = [{name: 'Dark Vader', imagePath: '/assets/images/temp/dark-vader.jpg', powerDescription: 'The force', description: 'Suspendisse eros mi, pretium sit amet elit ac, mollis convallis enim. Vivamus purus leo, faucibus ac rhoncus non, mattis at.', catchPhrase: 'Duis purus sem, porta nec turpis id, porta molestie lacus. Vivamus tellus est, tristique feugiat metus quis, pellentesque fringilla neque.'}, {name: 'Sauron', imagePath: '/assets/images/temp/sauron.jpg', powerDescription: 'Manipulation and mind control', description: 'Aenean quis ante posuere, placerat lorem dapibus, placerat est. Duis imperdiet risus tortor, a condimentum turpis congue vel. Duis vel.', catchPhrase: 'Suspendisse potenti. Morbi condimentum egestas risus ut consequat. Pellentesque hendrerit ex vel leo interdum tincidunt. Aenean et ipsum at dolor. '}, {name: 'Lord Voldemort', imagePath: '/assets/images/temp/voldemort.jpg', powerDescription: 'Dark magic', description: 'Phasellus egestas mattis leo, id laoreet sem ultrices vitae. Etiam nec purus mi. Vivamus tincidunt venenatis diam, non rutrum odio.', catchPhrase: 'Cras efficitur purus at justo faucibus viverra. In et mauris congue augue semper pellentesque et ac ligula. Maecenas sit amet.'}];

  constructor() { }

  ngOnInit(): void {
    AOS.init();
  }
}

class VilainProfile {
  name: String = '';
  imagePath: String = '';
  powerDescription: String = '';
  description: String = '';
  catchPhrase: String = '';
}
