import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-input-password',
  templateUrl: './input-password.component.html',
  styleUrls: ['./input-password.component.css'],
})
export class InputPasswordComponent implements OnInit, AfterViewInit {
  private _password: string | undefined = undefined;
  private _confirmPassword: string | undefined = undefined;

  @Output() outPassword = new EventEmitter<string | undefined>(undefined);
  @Output() outConfirmPassword = new EventEmitter<string | undefined>(undefined);
  @Input() submitted = false;
  @Input() autofocus = false;

  @ViewChild('inputPassword', {read: ElementRef}) inputPasswordElement: ElementRef<HTMLInputElement> | undefined;

  passwordMinLength = 6;
  passwordMaxLength = 20;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    if (this.autofocus) {
      this.inputPasswordElement?.nativeElement.focus();
    }
  }

  get password(): string | undefined {
    return this._password;
  }
  set password(password: string | undefined) {
    if (password == undefined ||
      (password.length && password.length >= this.passwordMinLength && password.length <= this.passwordMaxLength)) {
      this._password = password;
      this.outPassword.emit(this._password);

      if (this._confirmPassword == password) {
        this.outConfirmPassword.emit(this._confirmPassword);
      }
    }
  }

  get confirmPassword(): string | undefined {
    return this._confirmPassword;
  }
  set confirmPassword(confirmPassword: string | undefined) {
    this._confirmPassword = confirmPassword;
    if (this._password == confirmPassword) {
      this.outConfirmPassword.emit(this._confirmPassword);
    }
  }
}
