import {EventEmitter} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {InputPasswordComponent} from './input-password.component';

describe('InputPasswordComponent', () => {
  let component: InputPasswordComponent;
  let fixture: ComponentFixture<InputPasswordComponent>;
  let outPasswordSpy : jasmine.SpyObj<EventEmitter<string | undefined>>;
  let outConfirmPasswordSpy : jasmine.SpyObj<EventEmitter<string | undefined>>;

  beforeEach(async () => {
    outPasswordSpy = jasmine.createSpyObj<EventEmitter<string | undefined>>('EventEmitter', ['emit']);
    outConfirmPasswordSpy = jasmine.createSpyObj<EventEmitter<string | undefined>>('EventEmitter', ['emit']);

    await TestBed.configureTestingModule({
      declarations: [InputPasswordComponent],
      imports: [
        NgbModule,
        FormsModule,
      ],
    })
        .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputPasswordComponent);
    component = fixture.componentInstance;
    component.outPassword = outPasswordSpy;
    component.outConfirmPassword = outConfirmPasswordSpy;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('autofocus', () => {
    it('no autofocus', () => {
      component.autofocus = false;
      expect(fixture.debugElement.query(By.css(':focus'))).toBeUndefined;
    });
    it('with autofocus', () => {
      component.autofocus = true;
      component.ngAfterViewInit();
      const focusedElement = fixture.debugElement.query(By.css(':focus'));
      expect(fixture.debugElement.query(By.css(':focus'))).not.toBeUndefined;
      const inputPassword = fixture.debugElement.query(By.css('#inputPassword'));
      expect(focusedElement).toEqual(inputPassword);
    });
  });

  describe('password field', () => {
    it('empty and not dirty field', () => {
      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const inputPasswordError = compiled.querySelector('#inputPasswordError');
      expect(inputPasswordError).toBeNull();

      expect(component.password).toBeUndefined();
      expect(component.outPassword.emit).toHaveBeenCalledTimes(0);
    });

    it('empty and dirty field', () => {
      const compiled = fixture.nativeElement;
      const inputPassword = compiled.querySelector('#inputPassword');

      fixture.detectChanges();

      inputPassword.value = '';

      inputPassword.dispatchEvent(new Event('input'));
      inputPassword.dispatchEvent(new Event('touch'));

      fixture.detectChanges();

      const inputPasswordError = compiled.querySelector('#inputPasswordError');
      expect(inputPasswordError).not.toBeNull();
      expect(inputPasswordError.querySelector('div').textContent).toEqual('Password is required');

      expect(component.password).toBeUndefined();
      expect(component.outPassword.emit).toHaveBeenCalledTimes(0);
    });

    it('too short field', () => {
      const compiled = fixture.nativeElement;
      const inputPassword = compiled.querySelector('#inputPassword');

      fixture.detectChanges();

      inputPassword.value = 'a';

      inputPassword.dispatchEvent(new Event('input'));
      inputPassword.dispatchEvent(new Event('touch'));

      fixture.detectChanges();

      const inputPasswordError = compiled.querySelector('#inputPasswordError');
      expect(inputPasswordError).not.toBeNull();
      expect(inputPasswordError.querySelector('div').textContent).toEqual('Password must be longer than 6 characters');

      expect(component.password).toBeUndefined();
      expect(component.outPassword.emit).toHaveBeenCalledTimes(0);
    });

    it('too long field', () => {
      const compiled = fixture.nativeElement;
      const inputPassword = compiled.querySelector('#inputPassword');

      fixture.detectChanges();

      inputPassword.value = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';

      inputPassword.dispatchEvent(new Event('input'));
      inputPassword.dispatchEvent(new Event('touch'));

      fixture.detectChanges();

      const inputPasswordError = compiled.querySelector('#inputPasswordError');
      expect(inputPasswordError).not.toBeNull();
      expect(inputPasswordError.querySelector('div').textContent).toEqual('Password must be shorter than 20 characters');

      expect(component.password).toBeUndefined();
      expect(component.outPassword.emit).toHaveBeenCalledTimes(0);
    });

    it('Valid data', () => {
      const compiled = fixture.nativeElement;
      const inputPassword = compiled.querySelector('#inputPassword');

      fixture.detectChanges();

      inputPassword.value = 'password';

      inputPassword.dispatchEvent(new Event('input'));
      inputPassword.dispatchEvent(new Event('touch'));

      fixture.detectChanges();

      const inputPasswordError = compiled.querySelector('#inputPasswordError');
      expect(inputPasswordError).toBeNull();

      expect(component.password).toEqual(inputPassword.value);
      expect(component.outPassword.emit).toHaveBeenCalledOnceWith(inputPassword.value);
    });
  });

  describe('confirm password field', () => {
    it('empty and not dirty field', () => {
      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const confirmPasswordError = compiled.querySelector('#confirmInputPasswordError');
      expect(confirmPasswordError).toBeNull();

      expect(component.confirmPassword).toBeUndefined();
      expect(component.outConfirmPassword.emit).toHaveBeenCalledTimes(0);
    });

    it('empty and dirty field', () => {
      const compiled = fixture.nativeElement;
      const inputConfirmPassword = compiled.querySelector('#confirmInputPassword');

      fixture.detectChanges();

      inputConfirmPassword.value = '';

      inputConfirmPassword.dispatchEvent(new Event('input'));
      inputConfirmPassword.dispatchEvent(new Event('touch'));

      fixture.detectChanges();

      const inputConfirmPasswordError = compiled.querySelector('#confirmInputPasswordError');
      expect(inputConfirmPasswordError).not.toBeNull();
      expect(inputConfirmPasswordError.querySelector('div').textContent).toEqual('You must confirm your password');

      expect(component.confirmPassword).toEqual('');
      expect(component.outConfirmPassword.emit).toHaveBeenCalledTimes(0);
    });

    it('Incorrect data', () => {
      const compiled = fixture.nativeElement;
      const inputPassword = compiled.querySelector('#inputPassword');
      const inputConfirmPassword = compiled.querySelector('#confirmInputPassword');

      fixture.detectChanges();

      inputPassword.value = 'patate';

      inputPassword.dispatchEvent(new Event('input'));
      inputPassword.dispatchEvent(new Event('touch'));

      fixture.detectChanges();

      inputConfirmPassword.value = 'pasPatate';

      inputConfirmPassword.dispatchEvent(new Event('input'));
      inputConfirmPassword.dispatchEvent(new Event('touch'));

      fixture.detectChanges();

      const inputConfirmPasswordError = compiled.querySelector('#confirmInputPasswordError');
      expect(inputConfirmPasswordError).not.toBeNull();
      expect(inputConfirmPasswordError.querySelector('div').textContent).toEqual('Confirm password must be identical to the password');

      expect(component.confirmPassword).toEqual(inputConfirmPassword.value);
      expect(component.outConfirmPassword.emit).toHaveBeenCalledTimes(0);
    });

    it('Valid data', () => {
      const compiled = fixture.nativeElement;
      const inputPassword = compiled.querySelector('#inputPassword');
      const inputConfirmPassword = compiled.querySelector('#confirmInputPassword');

      const password = 'patate';

      fixture.detectChanges();

      inputPassword.value = password;

      inputPassword.dispatchEvent(new Event('input'));
      inputPassword.dispatchEvent(new Event('touch'));

      inputConfirmPassword.value = password;

      inputConfirmPassword.dispatchEvent(new Event('input'));
      inputConfirmPassword.dispatchEvent(new Event('touch'));

      fixture.detectChanges();

      const inputConfirmPasswordError = compiled.querySelector('#confirmInputPasswordError');
      expect(inputConfirmPasswordError).toBeNull();

      expect(component.confirmPassword).toEqual(inputConfirmPassword.value);

      expect(component.confirmPassword).toEqual(password);
      expect(component.outConfirmPassword.emit).toHaveBeenCalledOnceWith(password);
    });
  });
  describe('onSubmit', () => {
    it('Empty fields', () => {
      component.submitted = true;

      fixture.detectChanges();

      const compiled = fixture.nativeElement;

      const inputPasswordError = compiled.querySelector('#inputPasswordError');
      expect(inputPasswordError).not.toBeNull();
      expect(inputPasswordError.querySelector('div').textContent).toEqual('Password is required');

      const inputConfirmPasswordError = compiled.querySelector('#confirmInputPasswordError');
      expect(inputConfirmPasswordError).not.toBeNull();
      expect(inputConfirmPasswordError.querySelector('div').textContent).toEqual('You must confirm your password');

      expect(component.password).toBeUndefined();
      expect(component.outPassword.emit).toHaveBeenCalledTimes(0);
      expect(component.confirmPassword).toBeUndefined();
      expect(component.outConfirmPassword.emit).toHaveBeenCalledTimes(0);
    });
    it('Success', () => {
      const compiled = fixture.nativeElement;
      const password = 'patate';

      const inputConfirmPassword = compiled.querySelector('#confirmInputPassword');
      inputConfirmPassword.value = password;
      inputConfirmPassword.dispatchEvent(new Event('input'));

      const inputPassword = compiled.querySelector('#inputPassword');
      inputPassword.value = password;
      inputPassword.dispatchEvent(new Event('input'));

      component.submitted = true;

      fixture.detectChanges();

      const inputPasswordError = compiled.querySelector('#inputPasswordError');
      expect(inputPasswordError).toBeNull();

      const inputConfirmPasswordError = compiled.querySelector('#confirmInputPasswordError');
      expect(inputConfirmPasswordError).toBeNull();

      expect(component.password).toEqual(password);
      expect(component.outPassword.emit).toHaveBeenCalledOnceWith(password);
      expect(component.confirmPassword).toEqual(password);
      expect(component.outConfirmPassword.emit).toHaveBeenCalledOnceWith(password);
    });
  });
});
