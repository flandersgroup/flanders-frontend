import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-input-mail',
  templateUrl: './input-mail.component.html',
  styleUrls: ['./input-mail.component.css'],
})
export class InputMailComponent implements OnInit, AfterViewInit {
  private _email: string | undefined = undefined;

  @Output() outEmail = new EventEmitter<string | undefined>(undefined);
  @Input() submitted = false;
  @Input() autofocus = false;

  @ViewChild('inputEmail', {read: ElementRef}) inputEmailElement: ElementRef<HTMLInputElement> | undefined;

  private _emailPattern = '[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+';
  private regExp: RegExp = new RegExp(this.emailPattern);

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    if (this.autofocus) {
      this.inputEmailElement?.nativeElement.focus();
    }
  }

  get email(): string | undefined {
    return this._email;
  }
  set email(email: string | undefined) {
    if (email == undefined || this.regExp.test(email)) {
      this._email = email;
      this.outEmail.emit(this._email);
    }
  }
  get emailPattern() : string {
    return this._emailPattern;
  }
}
