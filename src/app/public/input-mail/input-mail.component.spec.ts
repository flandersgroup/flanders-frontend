import {EventEmitter} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {InputMailComponent} from './input-mail.component';

describe('InputMailComponent', () => {
  let component: InputMailComponent;
  let fixture: ComponentFixture<InputMailComponent>;
  let outputMailSpy : jasmine.SpyObj<EventEmitter<string | undefined>>;

  beforeEach(async () => {
    outputMailSpy = jasmine.createSpyObj<EventEmitter<string | undefined>>('EventEmitter', ['emit']);
    await TestBed.configureTestingModule({
      declarations: [InputMailComponent],
      imports: [
        NgbModule,
        FormsModule,
      ],
    })
        .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputMailComponent);
    component = fixture.componentInstance;
    component.outEmail = outputMailSpy;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('autofocus', () => {
    it('no autofocus', () => {
      component.autofocus = false;
      expect(fixture.debugElement.query(By.css(':focus'))).toBeUndefined;
    });
    it('with autofocus', () => {
      component.autofocus = true;
      component.ngAfterViewInit();
      const focusedElement = fixture.debugElement.query(By.css(':focus'));
      expect(fixture.debugElement.query(By.css(':focus'))).not.toBeUndefined;
      const inputEmail = fixture.debugElement.query(By.css('#inputEmail'));
      expect(focusedElement).toEqual(inputEmail);
    });
  });

  describe('email field', () => {
    it('empty and not dirty field', () => {
      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const inputEmailError = compiled.querySelector('#inputEmailError');
      expect(inputEmailError).toBeNull();

      expect(component.email).toBeUndefined();
      expect(outputMailSpy.emit).toHaveBeenCalledTimes(0);
    });

    it('empty and dirty field', () => {
      const compiled = fixture.nativeElement;
      const inputEmail = compiled.querySelector('#inputEmail');

      fixture.detectChanges();

      inputEmail.value = '';

      inputEmail.dispatchEvent(new Event('input'));
      inputEmail.dispatchEvent(new Event('touch'));

      fixture.detectChanges();

      const inputEmailError = compiled.querySelector('#inputEmailError');
      expect(inputEmailError).not.toBeNull();
      expect(inputEmailError.querySelector('div').textContent).toEqual('Email is required');

      expect(component.email).toBeUndefined();
      expect(outputMailSpy.emit).toHaveBeenCalledTimes(0);
    });

    it('Invalid email', () => {
      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const inputEmail = compiled.querySelector('#inputEmail');
      inputEmail.value = 'whatever';
      inputEmail.dispatchEvent(new Event('input'));

      fixture.detectChanges();

      const inputEmailError = compiled.querySelector('#inputEmailError');
      expect(inputEmailError).not.toBeNull();
      expect(inputEmailError.querySelector('div').textContent).toEqual('Please enter a valid email');

      expect(component.email).toBeUndefined();
      expect(outputMailSpy.emit).toHaveBeenCalledTimes(0);
    });

    it('Valid email', () => {
      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const inputEmail = compiled.querySelector('#inputEmail');
      inputEmail.value = 'test@test.com';
      inputEmail.dispatchEvent(new Event('input'));

      fixture.detectChanges();

      const inputEmailError = compiled.querySelector('#inputEmailError');
      expect(inputEmailError).toBeNull();

      expect(component.email).toEqual(inputEmail.value);
      expect(outputMailSpy.emit).toHaveBeenCalledOnceWith(inputEmail.value);
    });
  });
  describe('onSubmit', () => {
    it('Empty fields', () => {
      component.submitted = true;

      fixture.detectChanges();

      const compiled = fixture.nativeElement;

      const inputEmailError = compiled.querySelector('#inputEmailError');
      expect(inputEmailError).not.toBeNull();
      expect(inputEmailError.querySelector('div').textContent).toEqual('Email is required');

      expect(component.email).toBeUndefined();
      expect(outputMailSpy.emit).toHaveBeenCalledTimes(0);
    });
    it('Success', () => {
      const email = 'test@test.com';
      const compiled = fixture.nativeElement;
      const inputEmail = compiled.querySelector('#inputEmail');
      inputEmail.value = email;
      inputEmail.dispatchEvent(new Event('input'));

      component.submitted = true;

      fixture.detectChanges();

      const inputPasswordError = compiled.querySelector('#inputPasswordError');
      expect(inputPasswordError).toBeNull();

      const inputConfirmPasswordError = compiled.querySelector('#confirmInputPasswordError');
      expect(inputConfirmPasswordError).toBeNull();

      expect(component.email).toEqual(email);
      expect(outputMailSpy.emit).toHaveBeenCalledOnceWith(email);
    });
  });
});
