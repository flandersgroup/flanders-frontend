import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RegisteredParameters} from 'common/server/parameter/registered';
import {HomeResolverService} from 'src/app/service/home-resolver.service';
import {LoginService} from '../service/login.service';

@Component({
  selector: 'app-registered',
  templateUrl: './registered.component.html',
  styleUrls: ['./registered.component.css'],
})
export class RegisteredComponent implements OnInit {
  success = false;
  alreadyRegisteredError = false;
  error: { status: number, message: string } | undefined = undefined;

  constructor(
    private loginService: LoginService,
    private route: ActivatedRoute,
    private router: Router,
    private homeResolver: HomeResolverService) { }

  ngOnInit(): void {
    const tokenData = this.route.snapshot.queryParamMap.get('tokenData');

    this.router.navigate(
        [],
        {
          relativeTo: this.route,
          queryParams: null,
        });

    if (tokenData) {
      const registeredParameters = new RegisteredParameters;
      registeredParameters.tokenData = tokenData;
      this.loginService.registered(registeredParameters).subscribe(() => {
        this.success = true;
        this.error = undefined;
        this.alreadyRegisteredError = false;
      }, (error) => {
        this.success = false;
        this.error = {status: error.status, message: error.error};
        this.alreadyRegisteredError = error.error === 'User already registered';
      });
    } else {
      this.success = false;
      this.alreadyRegisteredError = false;
      this.error = {status: 400, message: 'Invalid data'};
    }
  }

  home(): void {
    this.homeResolver.userHome().subscribe((home) => this.router.navigate([home]));
  }
}
