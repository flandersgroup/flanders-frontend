import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivatedRoute, Router} from '@angular/router';
import {RegisteredParameters} from 'common/server/parameter/registered';
import {LoginResponse} from 'common/server/response/login';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {HomeResolverService} from 'src/app/service/home-resolver.service';
import {LoginService} from '../service/login.service';

import {RegisteredComponent} from './registered.component';

describe('RegisteredComponent', () => {
  let component: RegisteredComponent;
  let fixture: ComponentFixture<RegisteredComponent>;
  let loginServiceSpy: jasmine.SpyObj<LoginService>;
  let routerSpy: jasmine.SpyObj<Router>;
  let homeResolverSpy: jasmine.SpyObj<HomeResolverService>;
  let queryParamValue: string | undefined;

  beforeEach(async () => {
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    loginServiceSpy = jasmine.createSpyObj<LoginService>('LoginService', ['registered']);
    queryParamValue = 'tokenData';
    homeResolverSpy = jasmine.createSpyObj<HomeResolverService>('HomeResolverService', ['userHome']);

    const testbed = TestBed.configureTestingModule({
      declarations: [RegisteredComponent],
      providers: [
        {
          provide: HomeResolverService, useValue: homeResolverSpy,
        },
        {
          provide: LoginService, useValue: loginServiceSpy,
        },
        {
          provide: Router, useValue: routerSpy,
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              queryParamMap: {
                get: () => queryParamValue,
              },
            },
          },
        },
      ],
    });

    loginServiceSpy.registered.and.returnValue(new Observable());
    await testbed.compileComponents();
  });

  it('should create', () => {
    fixture = TestBed.createComponent(RegisteredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('initialy loading', () => {
    fixture = TestBed.createComponent(RegisteredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component.success).toBeFalse();
    expect(component.alreadyRegisteredError).toBeFalse();
    expect(component.error).toBeUndefined();

    const compiled = fixture.nativeElement;

    const loaderContainer = compiled.querySelector('#loaderContainer');
    expect(loaderContainer).not.toBeNull();

    const successContainer = compiled.querySelector('#successContainer');
    expect(successContainer).toBeNull();

    const errorContainer = compiled.querySelector('#errorContainer');
    expect(errorContainer).toBeNull();
  });

  it('success', () => {
    queryParamValue = 'tokenData';

    const userHome = new BehaviorSubject('mockHome');
    homeResolverSpy.userHome.and.returnValue(userHome);
    loginServiceSpy.registered.and.returnValue(new BehaviorSubject(new LoginResponse));

    fixture = TestBed.createComponent(RegisteredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const registeredParameters = new RegisteredParameters;
    registeredParameters.tokenData = queryParamValue;
    expect(loginServiceSpy.registered).toHaveBeenCalledOnceWith(registeredParameters);

    fixture.detectChanges();

    expect(component.success).toBeTrue();
    expect(component.alreadyRegisteredError).toBeFalse();
    expect(component.error).toBeUndefined();

    const compiled = fixture.nativeElement;

    const loaderContainer = compiled.querySelector('#loaderContainer');
    expect(loaderContainer).toBeNull();

    const errorContainer = compiled.querySelector('#errorContainer');
    expect(errorContainer).toBeNull();

    const successContainer = compiled.querySelector('#successContainer');
    expect(successContainer).not.toBeNull();

    expect(successContainer.querySelector('h1').textContent).toEqual( 'Welcome to the flanders company');
    expect(successContainer.querySelector('h3').textContent).toEqual('Your registration is successfull, you can now go to your home page');

    const link = successContainer.querySelector('h3').querySelector('a');
    expect(link).not.toBeNull();

    link.click();

    expect(routerSpy.navigate).toHaveBeenCalledWith(['mockHome']);
  });

  it('No query param', () => {
    queryParamValue = undefined;

    fixture = TestBed.createComponent(RegisteredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(loginServiceSpy.registered).toHaveBeenCalledTimes(0);

    fixture.detectChanges();

    expect(component.success).toBeFalse();
    expect(component.alreadyRegisteredError).toBeFalse();
    expect(component.error).toEqual({status: 400, message: 'Invalid data'});

    const compiled = fixture.nativeElement;

    const loaderContainer = compiled.querySelector('#loaderContainer');
    expect(loaderContainer).toBeNull();

    const successContainer = compiled.querySelector('#successContainer');
    expect(successContainer).toBeNull();

    const errorContainer = compiled.querySelector('#errorContainer');
    expect(errorContainer).not.toBeNull();

    expect(errorContainer.querySelector('h1').textContent).toEqual( 'Sorry an unexpected error occured');
    expect(errorContainer.querySelector('h3').textContent).toEqual( 'Next time it might work');
  });

  it('error', () => {
    queryParamValue = 'tokenData';
    loginServiceSpy.registered.and.returnValue(throwError({status: 409, error: 'Unexpected error'}));

    fixture = TestBed.createComponent(RegisteredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const registeredParameters = new RegisteredParameters;
    registeredParameters.tokenData = queryParamValue;
    expect(loginServiceSpy.registered).toHaveBeenCalledOnceWith(registeredParameters);

    fixture.detectChanges();

    expect(component.success).toBeFalse();
    expect(component.alreadyRegisteredError).toBeFalse();
    expect(component.error).toEqual({status: 409, message: 'Unexpected error'});

    const compiled = fixture.nativeElement;

    const loaderContainer = compiled.querySelector('#loaderContainer');
    expect(loaderContainer).toBeNull();

    const errorContainer = compiled.querySelector('#errorContainer');
    expect(errorContainer).not.toBeNull();

    expect(errorContainer.querySelector('h1').textContent).toEqual( 'Sorry an unexpected error occured');
    expect(errorContainer.querySelector('h3').textContent).toEqual( 'Next time it might work');

    const successContainer = compiled.querySelector('#successContainer');
    expect(successContainer).toBeNull();
  });

  it('User already registered', () => {
    queryParamValue = 'tokenData';
    loginServiceSpy.registered.and.returnValue(throwError({status: 409, error: 'User already registered'}));

    fixture = TestBed.createComponent(RegisteredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const registeredParameters = new RegisteredParameters;
    registeredParameters.tokenData = queryParamValue;
    expect(loginServiceSpy.registered).toHaveBeenCalledOnceWith(registeredParameters);

    fixture.detectChanges();

    expect(component.success).toBeFalse();
    expect(component.alreadyRegisteredError).toBeTrue();
    expect(component.error).toEqual({status: 409, message: 'User already registered'});

    const compiled = fixture.nativeElement;

    const loaderContainer = compiled.querySelector('#loaderContainer');
    expect(loaderContainer).toBeNull();

    const errorContainer = compiled.querySelector('#errorContainer');
    expect(errorContainer).not.toBeNull();

    expect(errorContainer.querySelector('h1').textContent).toEqual( 'You already registered to the website');
    expect(errorContainer.querySelector('h3').textContent).toEqual('Please go to the login page to login to the website');

    const successContainer = compiled.querySelector('#successContainer');
    expect(successContainer).toBeNull();
  });
});
