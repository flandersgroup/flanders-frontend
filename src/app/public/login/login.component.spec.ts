import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {of} from 'rxjs';
import {Observable} from 'rxjs/internal/Observable';
import {throwError} from 'rxjs/internal/observable/throwError';
import {HomeResolverService} from 'src/app/service/home-resolver.service';
import {InputMailComponent} from '../input-mail/input-mail.component';
import {LoginService} from '../service/login.service';

import {LoginComponent} from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let loginServiceSpy: jasmine.SpyObj<LoginService>;
  let routerSpy: jasmine.SpyObj<Router>;
  let homeResolverSpy: jasmine.SpyObj<HomeResolverService>;

  beforeEach(async () => {
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    loginServiceSpy = jasmine.createSpyObj<LoginService>('LoginService', ['login']);
    homeResolverSpy = jasmine.createSpyObj<HomeResolverService>('HomeResolverService', ['userHome']);

    const testbed = TestBed.configureTestingModule({
      declarations: [LoginComponent, InputMailComponent],
      providers: [
        {
          provide: HomeResolverService, useValue: homeResolverSpy,
        },
        {
          provide: LoginService, useValue: loginServiceSpy,
        },
        {
          provide: Router, useValue: routerSpy,
        }],
      imports: [
        NgbModule,
        FormsModule,
      ],
    });

    await testbed.compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    await fixture.whenStable();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('password field', () => {
    it('empty and not dirty field', () => {
      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const inputEmailError = compiled.querySelector('#inputPasswordError');
      expect(inputEmailError).toBeNull();
    });

    it('empty and dirty field', () => {
      const compiled = fixture.nativeElement;
      const inputPassword = compiled.querySelector('#inputPassword');

      fixture.detectChanges();

      inputPassword.value = '';
      inputPassword.dispatchEvent(new Event('input'));
      inputPassword.dispatchEvent(new Event('touch'));

      fixture.detectChanges();

      const inputPasswordError = fixture.nativeElement.querySelector('#inputPasswordError');
      expect(inputPasswordError).not.toBeNull();
      expect(inputPasswordError.querySelector('div').textContent).toEqual('Password is required');
    });

    it('Valid password', () => {
      const compiled = fixture.nativeElement;
      const inputPassword = compiled.querySelector('#inputPassword');
      inputPassword.value = 'password';
      inputPassword.dispatchEvent(new Event('input'));

      fixture.detectChanges();

      const inputPasswordError = compiled.querySelector('#inputPasswordError');
      expect(inputPasswordError).toBeNull();

      expect(component.password).toEqual(inputPassword.value);
    });
  });

  describe('onSubmit', () => {
    it('Empty fields', () => {
      expect(component.submitted).toBeFalse();

      fixture.detectChanges();

      const compiled = fixture.nativeElement;
      const getForm = fixture.debugElement.query(By.css('#loginForm'));
      expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();

      expect(component.submitted).toBeTrue();

      fixture.detectChanges();

      const inputEmail = fixture.debugElement.query(By.css('app-input-mail'));
      expect(inputEmail).not.toBeNull();
      const inputMailComponent = inputEmail.injector.get(InputMailComponent);
      expect(inputMailComponent.submitted).toBeTrue();

      const inputPasswordError = compiled.querySelector('#inputPasswordError');
      expect(inputPasswordError).not.toBeNull();
      expect(inputPasswordError.querySelector('div').textContent).toEqual('Password is required');

      expect(loginServiceSpy.login).toHaveBeenCalledTimes(0);
    });
    it('Sucess', () => {
      homeResolverSpy.userHome.and.returnValue(of('mockHome'));

      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const email = 'test@test.com';
      component.onEmail(email);

      const inputPassword = compiled.querySelector('#inputPassword');
      inputPassword.value = 'password';
      inputPassword.dispatchEvent(new Event('input'));

      fixture.detectChanges();

      loginServiceSpy.login.and.returnValue(new Observable((observer) => {
        observer.next();
      }));

      const getForm = fixture.debugElement.query(By.css('#loginForm'));
      expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();

      expect(loginServiceSpy.login).toHaveBeenCalledOnceWith(email, inputPassword.value);
      expect(routerSpy.navigate).toHaveBeenCalledOnceWith(['mockHome']);

      expect(compiled.querySelector('#errorToast')).toBeNull();
    });

    it('Failed', () => {
      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const email = 'test@test.com';
      component.onEmail(email);

      const password = 'password';
      const inputPassword = compiled.querySelector('#inputPassword');
      inputPassword.value = 'password';
      inputPassword.dispatchEvent(new Event('input'));

      fixture.detectChanges();

      loginServiceSpy.login.and.returnValue(throwError({error: 'Cannot login'}));

      const getForm = fixture.debugElement.query(By.css('#loginForm'));
      expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();

      fixture.detectChanges();

      expect(loginServiceSpy.login).toHaveBeenCalledOnceWith(email, password);

      const errorToast = fixture.nativeElement.querySelector('#errorToast');
      expect(errorToast).not.toBeNull;
      expect(errorToast.querySelector('span').textContent).toEqual('Cannot login');
    });
  });
});
