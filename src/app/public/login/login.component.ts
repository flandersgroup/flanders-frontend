import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../service/login.service';
import {HomeResolverService} from 'src/app/service/home-resolver.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  email: string | undefined = undefined;
  password: string | undefined = undefined;
  error: string | undefined = undefined;
  submitted = false;

  constructor(private loginService: LoginService,
    private router: Router,
    private homeResolver: HomeResolverService) { }

  ngOnInit(): void {
  }

  onEmail(email: string | undefined) {
    this.email = email;
  }

  onSubmit() {
    this.submitted = true;
    if (this.email && this.password) {
      this.loginService.login(this.email, this.password).subscribe(() => {
        this.homeResolver.userHome().subscribe((home) => this.router.navigate([home]));
      },
      (error) => {
        this.error = error.error;
      });
    }
  }
}
