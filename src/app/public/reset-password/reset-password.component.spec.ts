import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ResetPasswordParameters} from 'common/server/parameter/resetPassword';
import {Observable} from 'rxjs/internal/Observable';
import {throwError} from 'rxjs/internal/observable/throwError';
import {InputMailComponent} from '../input-mail/input-mail.component';
import {InputPasswordComponent} from '../input-password/input-password.component';
import {LoginService} from '../service/login.service';

import {ResetPasswordComponent} from './reset-password.component';

describe('ResetPasswordComponent', () => {
  let component: ResetPasswordComponent;
  let fixture: ComponentFixture<ResetPasswordComponent>;
  let loginServiceSpy: jasmine.SpyObj<LoginService>;
  let routerSpy: jasmine.SpyObj<Router>;
  let queryParamValue: string | undefined;

  beforeEach(async () => {
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    loginServiceSpy = jasmine.createSpyObj<LoginService>('LoginService', ['resetPassword']);
    queryParamValue = 'tokenData';
    const testbed = TestBed.configureTestingModule({
      declarations: [ResetPasswordComponent, InputPasswordComponent, InputMailComponent],
      providers: [
        {
          provide: LoginService, useValue: loginServiceSpy,
        },
        {
          provide: Router, useValue: routerSpy,
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              queryParamMap: {
                get: () => queryParamValue,
              },
            },
          },
        },
      ],
      imports: [
        NgbModule,
        FormsModule,
      ],
    });

    await testbed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onSubmit', () => {
    it('Empty fields', () => {
      expect(component.submitted).toBeFalse();

      fixture.detectChanges();

      const compiled = fixture.nativeElement;
      const getForm = fixture.debugElement.query(By.css('#resetPasswordForm'));
      expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();

      expect(component.submitted).toBeTrue();

      fixture.detectChanges();

      expect(loginServiceSpy.resetPassword).toHaveBeenCalledTimes(0);
    });
    it('No query param', () => {
      const compiled = fixture.nativeElement;
      queryParamValue = undefined;

      component.ngOnInit();

      fixture.detectChanges();

      const passwordValue ='patate';

      component.password = passwordValue;
      component.confirmPassword = passwordValue;

      fixture.detectChanges();

      loginServiceSpy.resetPassword.and.returnValue((new Observable((observer) => {
        observer.next();
      })));

      const getForm = fixture.debugElement.query(By.css('#resetPasswordForm'));
      expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();

      expect(loginServiceSpy.resetPassword).toHaveBeenCalledTimes(0);

      fixture.detectChanges();

      const errorToast = compiled.querySelector('#errorToast');
      expect(errorToast).not.toBeNull();
      expect(errorToast.querySelector('span').textContent).toEqual('Loading failed, please click again on the link in your email');
    });
    it('Success', () => {
      const compiled = fixture.nativeElement;
      queryParamValue = 'tokenData';

      component.ngOnInit();

      fixture.detectChanges();

      const passwordValue ='patate';

      component.password = passwordValue;
      component.confirmPassword = passwordValue;

      fixture.detectChanges();

      loginServiceSpy.resetPassword.and.returnValue((new Observable((observer) => {
        observer.next();
      })));

      const getForm = fixture.debugElement.query(By.css('#resetPasswordForm'));
      expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();

      const resetPasswordParameters = new ResetPasswordParameters;
      resetPasswordParameters.tokenData=queryParamValue;
      resetPasswordParameters.password=passwordValue;

      expect(loginServiceSpy.resetPassword).toHaveBeenCalledOnceWith(resetPasswordParameters);

      fixture.detectChanges();

      const successToast = compiled.querySelector('#successToast');
      expect(successToast).not.toBeNull();
      expect(successToast.querySelector('span').textContent).toEqual(
          'Your password have been successfully changed, you can now go to the login page to connect');
    });
    it('Failed', () => {
      const compiled = fixture.nativeElement;
      queryParamValue = 'tokenData';

      component.ngOnInit();

      fixture.detectChanges();

      const passwordValue ='patate';

      component.password = passwordValue;
      component.confirmPassword = passwordValue;

      fixture.detectChanges();

      loginServiceSpy.resetPassword.and.returnValue(throwError({error: 'Cannot register'}));

      const getForm = fixture.debugElement.query(By.css('#resetPasswordForm'));
      expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();

      const resetPasswordParameters = new ResetPasswordParameters;
      resetPasswordParameters.tokenData=queryParamValue;
      resetPasswordParameters.password=passwordValue;

      expect(loginServiceSpy.resetPassword).toHaveBeenCalledOnceWith(resetPasswordParameters);

      fixture.detectChanges();

      const errorToast = compiled.querySelector('#errorToast');
      expect(errorToast).not.toBeNull();
      expect(errorToast.querySelector('span').textContent).toEqual('Cannot register');
    });
  });
});
