import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ResetPasswordParameters} from 'common/server/parameter/resetPassword';
import {LoginService} from '../service/login.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css'],
})
export class ResetPasswordComponent implements OnInit {
  tokenData: string | null = null;
  password: string | undefined = undefined;
  confirmPassword: string | undefined = undefined;
  success = false;
  error: string | undefined = undefined;
  submitted = false;

  constructor(private loginService: LoginService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.tokenData = this.route.snapshot.queryParamMap.get('tokenData');

    this.router.navigate(
        [],
        {
          relativeTo: this.route,
          queryParams: null,
        });
  }

  onPassword(password: string | undefined) {
    this.password = password;
  }

  onConfirmPassword(confirmPassword: string | undefined) {
    this.confirmPassword = confirmPassword;
  }

  onSubmit() {
    this.submitted = true;
    if (this.tokenData == null) {
      this.error = 'Loading failed, please click again on the link in your email';
      this.success = false;
    } else if (this.password && this.confirmPassword == this.password) {
      const resetPasswordParameters = new ResetPasswordParameters;
      resetPasswordParameters.tokenData = this.tokenData;
      resetPasswordParameters.password = this.password;

      this.loginService.resetPassword(resetPasswordParameters).subscribe(() => {
        this.success = true;
        this.error = undefined;
      },
      (error) => {
        this.error = error.error;
        this.success = false;
      });
    }
  }
}
