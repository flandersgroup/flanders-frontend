import {Component, OnInit} from '@angular/core';
import {ResetPasswordRequestParameters} from 'common/server/parameter/resetPasswordRequest';
import {LoginService} from '../service/login.service';

@Component({
  selector: 'app-reset-password-request',
  templateUrl: './reset-password-request.component.html',
  styleUrls: ['./reset-password-request.component.css'],
})
export class ResetPasswordRequestComponent implements OnInit {
  email: string | undefined = undefined;
  error : string | undefined = undefined;
  success = false;
  submitted = false;

  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
  }

  onEmail(email : string | undefined) {
    this.email = email;
  }

  onSubmit() {
    this.submitted = true;
    if (this.email) {
      const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
      resetPasswordRequestParameters.email = this.email;
      this.loginService.resetPasswordRequest(resetPasswordRequestParameters).subscribe(() => {
        this.success=true;
        this.error = undefined;
      },
      (error) => {
        this.error = error.error;
        this.success =false;
      });
    }
  }
}
