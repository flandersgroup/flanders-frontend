import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ResetPasswordRequestParameters} from 'common/server/parameter/resetPasswordRequest';
import {Observable} from 'rxjs/internal/Observable';
import {throwError} from 'rxjs/internal/observable/throwError';
import {InputMailComponent} from '../input-mail/input-mail.component';
import {LoginService} from '../service/login.service';

import {ResetPasswordRequestComponent} from './reset-password-request.component';

describe('ResetPasswordRequestComponent', () => {
  let component: ResetPasswordRequestComponent;
  let fixture: ComponentFixture<ResetPasswordRequestComponent>;
  let loginServiceSpy: jasmine.SpyObj<LoginService>;

  beforeEach(async () => {
    loginServiceSpy = jasmine.createSpyObj<LoginService>('LoginService', ['resetPasswordRequest']);

    await TestBed.configureTestingModule({
      declarations: [ResetPasswordRequestComponent, InputMailComponent],
      providers: [
        {
          provide: LoginService, useValue: loginServiceSpy,
        },
      ],
      imports: [
        NgbModule,
        FormsModule,
      ],
    })
        .compileComponents();

    fixture = TestBed.createComponent(ResetPasswordRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onSubmit', () => {
    it('Empty fields', () => {
      expect(component.submitted).toBeFalse();

      fixture.detectChanges();

      const compiled = fixture.nativeElement;
      const getForm = fixture.debugElement.query(By.css('#ResetPasswordRequestForm'));
      expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();

      expect(component.submitted).toBeTrue();

      fixture.detectChanges();

      const inputEmail = fixture.debugElement.query(By.css('app-input-mail'));
      expect(inputEmail).not.toBeNull();
      const inputMailComponent = inputEmail.injector.get(InputMailComponent);
      expect(inputMailComponent.submitted).toBeTrue();
    });
    it('Success', () => {
      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const email = 'test@test.com';
      component.onEmail(email);

      fixture.detectChanges();

      loginServiceSpy.resetPasswordRequest.and.returnValue((new Observable((observer) => {
        observer.next();
      })));

      const getForm = fixture.debugElement.query(By.css('#ResetPasswordRequestForm'));
      expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();

      const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
      resetPasswordRequestParameters.email = email;

      expect(loginServiceSpy.resetPasswordRequest).toHaveBeenCalledOnceWith(resetPasswordRequestParameters);

      fixture.detectChanges();

      const successToast = compiled.querySelector('#successToast');
      expect(successToast).not.toBeNull();
      expect(successToast.querySelector('span').textContent).toEqual(
          'Reset password request submitted, you will receive a mail soon with instructions to reset your password');
    });
    it('Failed', () => {
      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const email = 'test@test.com';
      component.onEmail(email);

      loginServiceSpy.resetPasswordRequest.and.returnValue(throwError({error: 'Cannot find given email'}));

      const getForm = fixture.debugElement.query(By.css('#ResetPasswordRequestForm'));
      expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();

      const resetPasswordRequestParameters = new ResetPasswordRequestParameters;
      resetPasswordRequestParameters.email = email;

      expect(loginServiceSpy.resetPasswordRequest).toHaveBeenCalledOnceWith(resetPasswordRequestParameters);

      fixture.detectChanges();

      const errorToast = compiled.querySelector('#errorToast');
      expect(errorToast).not.toBeNull();
      expect(errorToast.querySelector('span').textContent).toEqual('Cannot find given email');
    });
  });
});
