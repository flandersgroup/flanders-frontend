import {Component, OnInit} from '@angular/core';
import {RegistrationParameters} from 'common/server/parameter/registration';
import {ProfileResponse} from 'common/server/response/profile';
import {LoginService} from '../service/login.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
  email: string | undefined = undefined;
  password: string | undefined = undefined;
  confirmPassword: string | undefined = undefined;
  success = false;
  error: string | undefined = undefined;
  profiles: ProfileResponse[] = [];
  selectedProfile: ProfileResponse | undefined = undefined;
  submitted = false;

  constructor(private loginService: LoginService) {
    this.loginService.registrableProfiles().subscribe((profiles) => {
      this.profiles = profiles;
    });
  }

  ngOnInit(): void {
  }

  selectProfile(profile: ProfileResponse) {
    this.selectedProfile = profile;
  }

  onEmail(email: string | undefined) {
    this.email = email;
  }

  onPassword(password: string | undefined) {
    this.password = password;
  }

  onConfirmPassword(confirmPassword: string | undefined) {
    this.confirmPassword = confirmPassword;
  }

  onSubmit() {
    this.submitted = true;
    if (this.email && this.password && this.confirmPassword == this.password && this.selectedProfile) {
      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = this.email;
      registrationParameters.password = this.password;
      registrationParameters.profile = this.selectedProfile.name;
      this.loginService.register(registrationParameters).subscribe(() => {
        this.success = true;
        this.error = undefined;
      },
      (error) => {
        this.error = error.error;
        this.success = false;
      });
    }
  }
}
