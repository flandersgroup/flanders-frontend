import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {By} from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RegistrationParameters} from 'common/server/parameter/registration';
import {ProfileResponse} from 'common/server/response/profile';
import {Observable, throwError} from 'rxjs';
import {InputMailComponent} from '../input-mail/input-mail.component';
import {InputPasswordComponent} from '../input-password/input-password.component';
import {LoginService} from '../service/login.service';

import {RegistrationComponent} from './registration.component';

describe('RegistrationComponent', () => {
  let component: RegistrationComponent;
  let fixture: ComponentFixture<RegistrationComponent>;
  let loginServiceSpy: jasmine.SpyObj<LoginService>;
  const profiles: ProfileResponse[] = [{name: 'Profile1', description: 'test1'},
    {name: 'Profile2', description: 'test2'}, {name: 'Profile3', description: 'test3'}];

  beforeEach(async () => {
    loginServiceSpy = jasmine.createSpyObj<LoginService>('LoginService', ['registrableProfiles', 'register']);

    loginServiceSpy.registrableProfiles.and.returnValue(new Observable<ProfileResponse[]>((observer) => {
      observer.next(profiles);
    }));

    await TestBed.configureTestingModule({
      declarations: [RegistrationComponent, InputPasswordComponent, InputMailComponent],
      providers: [
        {
          provide: LoginService, useValue: loginServiceSpy,
        },
      ],
      imports: [
        NgbModule,
        FormsModule,
      ],
    })
        .compileComponents();

    fixture = TestBed.createComponent(RegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('DropDown is filled with profiles', () => {
    expect(component.profiles).toEqual(profiles);

    const compiled = fixture.nativeElement;

    fixture.detectChanges();

    const dropdownProfiles = compiled.querySelector('#dropdownProfilesElement');

    expect(dropdownProfiles.childElementCount).toEqual(3);
    const childrens = dropdownProfiles.children;
    expect(childrens[0].textContent).toEqual('Profile1');
    expect(childrens[1].textContent).toEqual('Profile2');
    expect(childrens[2].textContent).toEqual('Profile3');
  });

  describe('Select profile drop down', () => {
    it('empty and not dirty field', () => {
      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const selectProfileError = compiled.querySelector('#selectProfileError');
      expect(selectProfileError).toBeNull();
    });
    it('select profile', () => {
      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const dropdownProfilesButton = compiled.querySelector('#dropdownProfilesButton');
      expect(dropdownProfilesButton).not.toBeNull();
      expect(dropdownProfilesButton.textContent).toEqual('Select a profile');
      dropdownProfilesButton.click();

      const dropdownProfilesElement = compiled.querySelector('#dropdownProfilesElement');
      expect(dropdownProfilesElement).not.toBeNull();
      expect(dropdownProfilesElement.childElementCount).toEqual(3);
      const childrens = dropdownProfilesElement.children;
      expect(childrens[0].textContent).toEqual('Profile1');
      childrens[0].click();

      fixture.detectChanges();

      expect(dropdownProfilesButton.textContent).toEqual('Profile1');
      expect(component.selectedProfile).not.toBeUndefined();
      expect(component.selectedProfile?.name).toEqual('Profile1');
      expect(component.selectedProfile?.description).toEqual('test1');

      expect(childrens[1].textContent).toEqual('Profile2');
      childrens[1].click();

      fixture.detectChanges();

      expect(dropdownProfilesButton.textContent).toEqual('Profile2');
      expect(component.selectedProfile).not.toBeUndefined();
      expect(component.selectedProfile?.name).toEqual('Profile2');
      expect(component.selectedProfile?.description).toEqual('test2');
    });
  });
  describe('onSubmit', () => {
    it('Empty fields', () => {
      expect(component.submitted).toBeFalse();

      fixture.detectChanges();

      const compiled = fixture.nativeElement;
      const getForm = fixture.debugElement.query(By.css('#registrationForm'));
      expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();

      expect(component.submitted).toBeTrue();

      fixture.detectChanges();

      const inputEmailError = compiled.querySelector('#inputEmailError');
      expect(inputEmailError).not.toBeNull();
      expect(inputEmailError.querySelector('div').textContent).toEqual('Email is required');

      const selectProfileError = compiled.querySelector('#selectProfileError');
      expect(selectProfileError).not.toBeNull();
      expect(selectProfileError.textContent).toEqual('Please select a profile');

      expect(loginServiceSpy.register).toHaveBeenCalledTimes(0);
    });
    it('Success', () => {
      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const inputEmail = compiled.querySelector('#inputEmail');
      inputEmail.value = 'test@test.com';
      inputEmail.dispatchEvent(new Event('input'));

      component.onPassword('patate');
      component.onConfirmPassword('patate');

      const dropdownProfilesElement = compiled.querySelector('#dropdownProfilesElement');
      const childrens = dropdownProfilesElement.children;
      expect(childrens[0].textContent).toEqual('Profile1');
      childrens[0].click();

      fixture.detectChanges();

      loginServiceSpy.register.and.returnValue((new Observable((observer) => {
        observer.next();
      })));

      const getForm = fixture.debugElement.query(By.css('#registrationForm'));
      expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();

      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'test@test.com';
      registrationParameters.password = 'patate';
      registrationParameters.profile = 'Profile1';

      expect(loginServiceSpy.register).toHaveBeenCalledOnceWith(registrationParameters);

      fixture.detectChanges();

      const successToast = compiled.querySelector('#successToast');
      expect(successToast).not.toBeNull();
      expect(successToast.querySelector('span').textContent).toEqual(
          'Registration succeed, you will receive a mail soon to confirm your registration');
    });
    it('Failed', () => {
      const compiled = fixture.nativeElement;

      fixture.detectChanges();

      const inputEmail = compiled.querySelector('#inputEmail');
      inputEmail.value = 'test@test.com';
      inputEmail.dispatchEvent(new Event('input'));

      component.onPassword('patate');
      component.onConfirmPassword('patate');

      const dropdownProfilesElement = compiled.querySelector('#dropdownProfilesElement');
      const childrens = dropdownProfilesElement.children;
      expect(childrens[0].textContent).toEqual('Profile1');
      childrens[0].click();

      fixture.detectChanges();

      loginServiceSpy.register.and.returnValue(throwError({error: 'Cannot register'}));

      const getForm = fixture.debugElement.query(By.css('#registrationForm'));
      expect(getForm.triggerEventHandler('submit', compiled)).toBeUndefined();

      const registrationParameters = new RegistrationParameters;
      registrationParameters.email = 'test@test.com';
      registrationParameters.password = 'patate';
      registrationParameters.profile = 'Profile1';

      expect(loginServiceSpy.register).toHaveBeenCalledOnceWith(registrationParameters);

      fixture.detectChanges();

      const errorToast = compiled.querySelector('#errorToast');
      expect(errorToast).not.toBeNull();
      expect(errorToast.querySelector('span').textContent).toEqual('Cannot register');
    });
  });
});
