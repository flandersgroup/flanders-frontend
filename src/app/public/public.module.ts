import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PublicRoutingModule} from './public-routing.module';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {RegistrationComponent} from './registration/registration.component';
import {RegisteredComponent} from './registered/registered.component';
import {ResetPasswordRequestComponent} from './reset-password-request/reset-password-request.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {InputPasswordComponent} from './input-password/input-password.component';
import {InputMailComponent} from './input-mail/input-mail.component';

@NgModule({
  declarations: [LoginComponent, HomeComponent, RegistrationComponent,
    RegisteredComponent, ResetPasswordRequestComponent, ResetPasswordComponent,
    InputPasswordComponent, InputMailComponent],
  imports: [
    CommonModule,
    PublicRoutingModule,
    NgbModule,
    FormsModule,
  ],
})
export class PublicModule { }
