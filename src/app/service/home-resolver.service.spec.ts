import {TestBed} from '@angular/core/testing';
import {Authorizations} from 'common/model/authorizations';
import {AuthorizationService} from './authorization.service';

import {HomeResolverService} from './home-resolver.service';

describe('HomeResolverService', () => {
  let service: HomeResolverService;
  let authorizationServiceSpy: jasmine.SpyObj<AuthorizationService>;

  beforeEach(() => {
    authorizationServiceSpy = jasmine.createSpyObj<AuthorizationService>('AuthorizationService', ['authorization']);
    TestBed.configureTestingModule({
      providers: [HomeResolverService,
        {provide: AuthorizationService, useValue: authorizationServiceSpy}],
    });
    service = TestBed.inject(HomeResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('CanAdministrateUsers', (done) => {
    authorizationServiceSpy.authorization.and.returnValue(false);
    authorizationServiceSpy.authorization.withArgs(Authorizations.CanAdministrateUsers).and.returnValue(true);
    service.userHome().subscribe(
        (userHome) => {
          expect(userHome).toEqual('admin/users');
          done();
        },
        fail,
    );
  });

  it('CanSeeHisSubmittedMissions', (done) => {
    authorizationServiceSpy.authorization.and.returnValue(false);
    authorizationServiceSpy.authorization.withArgs(Authorizations.CanSeeHisSubmittedMissions).and.returnValue(true);
    service.userHome().subscribe(
        (userHome) => {
          expect(userHome).toEqual('hero/mymissions');
          done();
        },
        fail,
    );
  });

  it('CanSeeHisSubmittedMissions', (done) => {
    authorizationServiceSpy.authorization.and.returnValue(false);
    authorizationServiceSpy.authorization.withArgs(Authorizations.CanSubmitMission).and.returnValue(true);
    service.userHome().subscribe(
        (userHome) => {
          expect(userHome).toEqual('hero/submitmission');
          done();
        },
        fail,
    );
  });

  it('CanSeeHisSubmittedMissions', (done) => {
    authorizationServiceSpy.authorization.and.returnValue(false);
    authorizationServiceSpy.authorization.withArgs(Authorizations.CanSeeHisSubmittedMissions).and.returnValue(true);
    authorizationServiceSpy.authorization.withArgs(Authorizations.CanSubmitMission).and.returnValue(true);
    service.userHome().subscribe(
        (userHome) => {
          expect(userHome).toEqual('hero/mymissions');
          done();
        },
        fail,
    );
  });

  it('no authorization', (done) => {
    authorizationServiceSpy.authorization.and.returnValue(false);
    service.userHome().subscribe(
        (userHome) => {
          expect(userHome).toEqual('');
          done();
        },
        fail,
    );
  });
});
