import {HttpClient, HttpParams} from '@angular/common/http';
import {AuthorizationResponse} from 'common/server/response/authorization';
import {ProfileResponse} from 'common/server/response/profile';
import {Subject, throwError} from 'rxjs';
import {environment} from 'src/environments/environment';

import {AuthenticationService} from './authentication.service';
import {AuthorizationService} from './authorization.service';

describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let authorizationServiceSpy: jasmine.SpyObj<AuthorizationService>;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let profileObservable = new Subject<ProfileResponse[]>();
  let authorizationObservable = new Subject<AuthorizationResponse[]>();

  let tokenSpyGetter : jasmine.Spy<jasmine.Func>;
  let tokenSpySetter : jasmine.Spy<jasmine.Func>;

  const profilesResponse: ProfileResponse[] = [];

  const adminProfile = new ProfileResponse;
  adminProfile.name = 'Administrator';
  adminProfile.description = 'Administrator of the website, able to administrate users';
  profilesResponse.push(adminProfile);

  const vilainProfile = new ProfileResponse;
  vilainProfile.name = 'Vilain';
  vilainProfile.description = 'A vilain who can edit his profile and receive missions';
  profilesResponse.push(vilainProfile);

  const authorizationsReponse: AuthorizationResponse[] = [];

  const seeMissionAuthorization = new AuthorizationResponse;
  seeMissionAuthorization.name = 'CanSeeHisSubmittedMissions';
  seeMissionAuthorization.description = 'Can consult the missions he submitted';
  authorizationsReponse.push(seeMissionAuthorization);

  const submitMissionAuthorization = new AuthorizationResponse;
  submitMissionAuthorization.name = 'CanSubmitMission';
  submitMissionAuthorization.description = 'Can submit mission';
  authorizationsReponse.push(submitMissionAuthorization);

  beforeEach(() => {
    localStorage.clear();
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    authorizationServiceSpy = jasmine.createSpyObj('authorizationService',
        ['authorizations', 'authorizationsRequestResponse', 'logout'], {authenticationToken: null});

    tokenSpyGetter = Object.getOwnPropertyDescriptor(authorizationServiceSpy, 'authenticationToken')?.get as jasmine.Spy<jasmine.Func>;
    tokenSpySetter = Object.getOwnPropertyDescriptor(authorizationServiceSpy, 'authenticationToken')?.set as jasmine.Spy<jasmine.Func>;

    profileObservable = new Subject<ProfileResponse[]>();
    authorizationObservable = new Subject<AuthorizationResponse[]>();

    httpClientSpy.get.and.returnValues(profileObservable, authorizationObservable);

    service = new AuthenticationService(authorizationServiceSpy, httpClientSpy);
  });

  it('authentication when no user_access exists', () => {
    service = new AuthenticationService(authorizationServiceSpy, httpClientSpy);
    expect(httpClientSpy.get).toHaveBeenCalledTimes(0);
    expect(tokenSpySetter).toHaveBeenCalledTimes(0);
  });

  describe('authentication whith user_access exists', () => {
    it('no profiles', () => {
      tokenSpyGetter.and.returnValue('aToken');
      service = new AuthenticationService(authorizationServiceSpy, httpClientSpy);

      expect(httpClientSpy.get).toHaveBeenCalledOnceWith(environment.apiUrl + '/profilesOfUser');

      profileObservable.next([]);

      expect(authorizationServiceSpy.logout).toHaveBeenCalledTimes(1);
      expect(authorizationServiceSpy.authorizationsRequestResponse).toBeUndefined();
      expect(authorizationServiceSpy.profiles).toBeUndefined();
      expect(authorizationServiceSpy.selectedProfile).toBeUndefined();
    });
    it('multiple profiles', () => {
      tokenSpyGetter.and.returnValue('aToken');
      service = new AuthenticationService(authorizationServiceSpy, httpClientSpy);

      expect(httpClientSpy.get).toHaveBeenCalledOnceWith(environment.apiUrl + '/profilesOfUser');

      expect(authorizationServiceSpy.authorizationsRequestResponse).toBeInstanceOf(Subject);
      expect(authorizationServiceSpy.authorizationsRequestResponse).not.toBeUndefined();
      expect(authorizationServiceSpy.authorizations.length).toEqual(0);
      expect(authorizationServiceSpy.profiles).toBeUndefined();

      authorizationServiceSpy.authorizationsRequestResponse?.subscribe((authorizations) => {
        expect(authorizations).toEqual(authorizationsReponse);
      });

      profileObservable.next(profilesResponse);

      expect(authorizationServiceSpy.authorizationsRequestResponse).toBeInstanceOf(Subject);
      expect(authorizationServiceSpy.authorizationsRequestResponse).not.toBeUndefined();
      expect(authorizationServiceSpy.authorizations.length).toEqual(0);
      expect(authorizationServiceSpy.profiles).toEqual(profilesResponse);
      expect(authorizationServiceSpy.selectedProfile).toEqual(adminProfile);

      authorizationObservable.next(authorizationsReponse);

      expect(httpClientSpy.get).toHaveBeenCalledTimes(2);

      const options = {params: new HttpParams().set('profileName', profilesResponse[0].name)};
      expect(httpClientSpy.get).toHaveBeenCalledWith(environment.apiUrl + '/authorizationsOfProfile', options);

      expect(authorizationServiceSpy.authorizations).toEqual(authorizationsReponse);
      expect(authorizationServiceSpy.authorizationsRequestResponse).toBeUndefined();
      expect(authorizationServiceSpy.profiles).toEqual(profilesResponse);
      expect(authorizationServiceSpy.selectedProfile).toEqual(adminProfile);
    });
  });

  describe('selectProfile', () => {
    it('failed', () => {
      httpClientSpy.get.and.returnValue(throwError({status: 404}));

      service.selectProfile(adminProfile);

      const options = {params: new HttpParams().set('profileName', adminProfile.name)};
      expect(httpClientSpy.get).toHaveBeenCalledWith(environment.apiUrl + '/authorizationsOfProfile', options);

      expect(authorizationServiceSpy.logout).toHaveBeenCalledTimes(1);
      expect(authorizationServiceSpy.authorizationsRequestResponse).toBeUndefined();
      expect(authorizationServiceSpy.authorizations.length).toEqual(0);
      expect(authorizationServiceSpy.profiles).toBeUndefined();
    });
    it('success', () => {
      httpClientSpy.get.and.returnValue(authorizationObservable);

      expect(authorizationServiceSpy.selectedProfile).toBeUndefined();

      service.selectProfile(adminProfile);

      const options = {params: new HttpParams().set('profileName', adminProfile.name)};
      expect(httpClientSpy.get).toHaveBeenCalledWith(environment.apiUrl + '/authorizationsOfProfile', options);

      expect(authorizationServiceSpy.authorizationsRequestResponse).toBeInstanceOf(Subject);
      expect(authorizationServiceSpy.authorizationsRequestResponse).not.toBeUndefined();
      expect(authorizationServiceSpy.authorizations.length).toEqual(0);

      authorizationObservable.next(authorizationsReponse);

      expect(authorizationServiceSpy.authorizations).toEqual(authorizationsReponse);
      expect(authorizationServiceSpy.authorizationsRequestResponse).toBeUndefined();
      expect(authorizationServiceSpy.selectedProfile).toEqual(adminProfile);
    });
  });

  describe('login', () => {
    it('no profiles', () => {
      const requestToken = 'fakeToken';
      service.login(requestToken);

      expect(httpClientSpy.get).toHaveBeenCalledOnceWith(environment.apiUrl + '/profilesOfUser');

      expect(tokenSpySetter).toHaveBeenCalledOnceWith(requestToken);
      expect(authorizationServiceSpy.authorizationsRequestResponse).toBeInstanceOf(Subject);
      expect(authorizationServiceSpy.authorizationsRequestResponse).not.toBeUndefined();
      expect(authorizationServiceSpy.authorizations.length).toEqual(0);

      authorizationServiceSpy.authorizationsRequestResponse?.subscribe((authorizations) => {
        expect(authorizations).toEqual(authorizationsReponse);
      });

      profileObservable.next([]);

      expect(authorizationServiceSpy.logout).toHaveBeenCalledTimes(1);
      expect(authorizationServiceSpy.authorizationsRequestResponse).toBeUndefined();
      expect(authorizationServiceSpy.authorizations.length).toEqual(0);
      expect(authorizationServiceSpy.profiles).toBeUndefined();
      expect(authorizationServiceSpy.selectedProfile).toBeUndefined();
    });
    it('multiple profiles', () => {
      const requestToken = 'fakeToken';
      service.login(requestToken);

      expect(httpClientSpy.get).toHaveBeenCalledOnceWith(environment.apiUrl + '/profilesOfUser');

      expect(tokenSpySetter).toHaveBeenCalledOnceWith(requestToken);
      expect(authorizationServiceSpy.authorizationsRequestResponse).toBeInstanceOf(Subject);
      expect(authorizationServiceSpy.authorizationsRequestResponse).not.toBeUndefined();
      expect(authorizationServiceSpy.authorizations.length).toEqual(0);

      authorizationServiceSpy.authorizationsRequestResponse?.subscribe((authorizations) => {
        expect(authorizations).toEqual(authorizationsReponse);
      });

      expect(authorizationServiceSpy.selectedProfile).toBeUndefined();
      profileObservable.next(profilesResponse);
      expect(authorizationServiceSpy.selectedProfile).toEqual(adminProfile);

      expect(authorizationServiceSpy.authorizationsRequestResponse).toBeInstanceOf(Subject);
      expect(authorizationServiceSpy.authorizationsRequestResponse).not.toBeUndefined();
      expect(authorizationServiceSpy.authorizations.length).toEqual(0);
      expect(authorizationServiceSpy.profiles).toEqual(profilesResponse);

      authorizationObservable.next(authorizationsReponse);

      expect(httpClientSpy.get).toHaveBeenCalledTimes(2);

      const options = {params: new HttpParams().set('profileName', profilesResponse[0].name)};
      expect(httpClientSpy.get).toHaveBeenCalledWith(environment.apiUrl + '/authorizationsOfProfile', options);

      expect(authorizationServiceSpy.authorizations).toEqual(authorizationsReponse);
      expect(authorizationServiceSpy.authorizationsRequestResponse).toBeUndefined();
      expect(authorizationServiceSpy.profiles).toEqual(profilesResponse);
      expect(authorizationServiceSpy.selectedProfile).toEqual(adminProfile);
    });
  });

  it('login failed', () => {
    httpClientSpy.get.and.returnValue(throwError({status: 404}));

    const requestToken = 'fakeToken';
    service.login(requestToken);

    expect(authorizationServiceSpy.authorizationsRequestResponse).toBeUndefined();
    expect(authorizationServiceSpy.authorizations.length).toEqual(0);
    expect(httpClientSpy.get).toHaveBeenCalledOnceWith(environment.apiUrl + '/profilesOfUser');

    expect(authorizationServiceSpy.logout).toHaveBeenCalledTimes(1);
    expect(authorizationServiceSpy.authorizationsRequestResponse).toBeUndefined();
    expect(authorizationServiceSpy.authorizations.length).toEqual(0);
    expect(authorizationServiceSpy.profiles).toBeUndefined();
    expect(authorizationServiceSpy.selectedProfile).toBeUndefined();
  });

  it('logout', () => {
    service.logout();
    expect(authorizationServiceSpy.logout).toHaveBeenCalledTimes(1);
  });


  afterEach(() => {
    localStorage.clear();
  });
});
