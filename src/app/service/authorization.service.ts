import {Injectable} from '@angular/core';
import {AuthorizationResponse} from 'common/server/response/authorization';
import {ProfileResponse} from 'common/server/response/profile';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthorizationService {
  private _authorizationsRequestResponse: Observable<AuthorizationResponse[]> | undefined = undefined;
  private _authorizations: AuthorizationResponse[] = [];
  private _profiles : ProfileResponse[] = [];
  private _selectedProfile: ProfileResponse | undefined = undefined;

  constructor() { }

  logout(): void {
    this.authenticationToken = null;
    this._authorizations = [];
    this._authorizationsRequestResponse = undefined;
    this._profiles= [];
    this._selectedProfile = undefined;
  }

  authorization(authorizations: string | string[]): boolean | Observable<boolean> {
    if (this._authorizationsRequestResponse) {
      const result = new Subject<boolean>();
      this._authorizationsRequestResponse.subscribe((resultats) => {
        this._authorizations = resultats;
        result.next(this._authorization(authorizations));
      });
      return result;
    } else {
      return this._authorization(authorizations);
    }
  }

  isAuthenticated(): boolean {
    return this.authenticationToken !== null;
  }

  private _authorization(authorizations: string | string[]): boolean {
    let authorized = true;
    if (authorizations instanceof Array) {
      for (const authorization of authorizations) {
        if (this._authorizations.findIndex((value) => {
          return value.name === authorization;
        }) === -1) {
          authorized = false;
        }
      }
    } else {
      authorized = this._authorizations.findIndex((value) => {
        return value.name === authorizations;
      }) !== -1;
    }
    return authorized;
  }

  get authorizationsRequestResponse(): Observable<AuthorizationResponse[]> | undefined {
    return this._authorizationsRequestResponse;
  }

  get authenticationToken(): string | null {
    return localStorage.getItem('user_access');
  }

  set authenticationToken(token: string | null) {
    if (token) {
      localStorage.setItem('user_access', token);
    } else {
      localStorage.clear();
    }
  }

  set authorizationsRequestResponse(value: Observable<AuthorizationResponse[]> | undefined) {
    this._authorizationsRequestResponse = value;
  }

  get authorizations(): AuthorizationResponse[] {
    return this._authorizations;
  }

  set authorizations(value: AuthorizationResponse[]) {
    this._authorizations = value;
  }

  get profiles() : ProfileResponse[] {
    return this._profiles;
  }

  set profiles(profiles : ProfileResponse[]) {
    this._profiles = profiles;
  }

  get selectedProfile(): ProfileResponse | undefined {
    return this._selectedProfile;
  }

  set selectedProfile(selectedProfile: ProfileResponse | undefined) {
    this._selectedProfile = selectedProfile;
  }
}
