import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AuthorizationResponse} from 'common/server/response/authorization';
import {ProfileResponse} from 'common/server/response/profile';
import {Subject} from 'rxjs';
import {environment} from 'src/environments/environment';
import {AuthorizationService} from './authorization.service';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private _authorizationService: AuthorizationService, private _httpClient: HttpClient) {
    if (this._authorizationService.authenticationToken) {
      this._getAuthorizations();
    }
  }

  login(token: string): void {
    this._authorizationService.authenticationToken = token;
    this._getAuthorizations();

    if (this._authorizationService.authorizations === []) {
      this.logout();
    }
  }

  logout(): void {
    this._authorizationService.logout();
  }

  selectProfile(profile: ProfileResponse): void {
    this._authorizationService.authorizationsRequestResponse = new Subject<AuthorizationResponse[]>();
    this._selectProfile(profile);
  }

  private _getAuthorizations(): void {
    this._authorizationService.authorizationsRequestResponse = new Subject<AuthorizationResponse[]>();

    this._httpClient.get<ProfileResponse[]>(environment.apiUrl + '/profilesOfUser').subscribe((profiles) => {
      if (profiles.length > 0) {
        this._authorizationService.profiles = profiles;
        this._selectProfile(profiles[0]);
      } else {
        this.logout();
        this._authorizationService.authorizationsRequestResponse = undefined;
      }
    }, () => {
      this.logout();
      this._authorizationService.authorizationsRequestResponse = undefined;
    });
  }

  private _selectProfile(profile: ProfileResponse): void {
    this._authorizationService.selectedProfile = profile;
    const options = {params: new HttpParams().set('profileName', profile.name)};
    this._httpClient.get<AuthorizationResponse[]>(environment.apiUrl + '/authorizationsOfProfile', options).subscribe((authorizations) => {
      this._authorizationService.authorizations = authorizations;
      (this._authorizationService.authorizationsRequestResponse as Subject<AuthorizationResponse[]>).next(
          this._authorizationService.authorizations);
      this._authorizationService.authorizationsRequestResponse = undefined;
    }, () => {
      this.logout();
      this._authorizationService.authorizationsRequestResponse = undefined;
    });
  }
}
