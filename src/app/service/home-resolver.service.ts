import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, ReplaySubject} from 'rxjs';
import {Authorizations} from 'common/model/authorizations';
import {AuthorizationService} from './authorization.service';

@Injectable({
  providedIn: 'root',
})
export class HomeResolverService {
  constructor(private authorizationService : AuthorizationService) { }

  userHome(): Observable<string> {
    const result = new ReplaySubject<string>();

    const authorizationMap = new Map<Observable<Boolean>, string>();
    authorizationMap.set(this._authorized(Authorizations.CanAdministrateUsers), 'admin/users');
    authorizationMap.set(this._authorized(Authorizations.CanSeeHisSubmittedMissions), 'hero/mymissions');
    authorizationMap.set(this._authorized(Authorizations.CanSubmitMission), 'hero/submitmission');

    authorizationMap.forEach((value, key) => {
      key.subscribe((authorized) => {
        if (authorizationMap.size !== 0) {
          if (authorized) {
            result.next(value);
            authorizationMap.clear();
          } else {
            authorizationMap.delete(key);
            if (authorizationMap.size === 0) {
              result.next('');
            }
          }
        }
      },
      (error) => result.error(error));
    });

    return result;
  }

  private _authorized(authorizations: string | string[]): Observable<Boolean> {
    let result: Observable<Boolean>;
    const authorization = this.authorizationService.authorization(authorizations);
    if (authorization instanceof Observable) {
      result = authorization;
    } else {
      result = new BehaviorSubject<Boolean>(authorization);
    }

    return result;
  }
}
