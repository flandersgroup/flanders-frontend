import {TestBed} from '@angular/core/testing';
import {AuthorizationResponse} from 'common/server/response/authorization';
import {ProfileResponse} from 'common/server/response/profile';
import {Observable, Subject} from 'rxjs';

import {AuthorizationService} from './authorization.service';

describe('AuthorizationService', () => {
  let service: AuthorizationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthorizationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Authentication token', () => {
    expect(service.authenticationToken).toBeNull();
    expect(service.isAuthenticated()).toBeFalse();

    const token = 'aToken';
    service.authenticationToken = token;
    expect(service.authenticationToken).toEqual(token);
    expect(service.isAuthenticated()).toBeTrue();

    service.authenticationToken = null;
    expect(service.authenticationToken).toBeNull();
    expect(service.isAuthenticated()).toBeFalse();
  });

  it('Authorization synchronous', () => {
    expect(service.authorizations).toEqual([]);

    expect(service.authorization('whatever')).toBeFalse();
    expect(service.authorization(['whatever1', 'whatever2'])).toBeFalse();

    const expectedAuthorizations = [];

    const authorization0 = new AuthorizationResponse;
    authorization0.name = 'CanSeeHisSubmittedMissions';
    authorization0.description = 'Can consult the missions he submitted';
    expectedAuthorizations.push(authorization0);

    const authorization1 = new AuthorizationResponse;
    authorization1.name = 'CanSubmitMission';
    authorization1.description = 'Can submit mission';
    expectedAuthorizations.push(authorization1);

    service.authorizations = expectedAuthorizations;

    expect(service.authorization(authorization0.name)).toBeTrue();
    expect(service.authorization(authorization1.name)).toBeTrue();
    expect(service.authorization('whatever')).toBeFalse();
    expect(service.authorization([authorization0.name, authorization1.name])).toBeTrue();
    expect(service.authorization([authorization0.name, 'whatever'])).toBeFalse();
  });

  it('Authorization synchronous', () => {
    expect(service.authorizations).toEqual([]);

    expect(service.authorization('whatever')).toBeFalse();
    expect(service.authorization(['whatever1', 'whatever2'])).toBeFalse();

    const expectedAuthorizations = [];

    const authorization0 = new AuthorizationResponse;
    authorization0.name = 'CanSeeHisSubmittedMissions';
    authorization0.description = 'Can consult the missions he submitted';
    expectedAuthorizations.push(authorization0);

    const authorization1 = new AuthorizationResponse;
    authorization1.name = 'CanSubmitMission';
    authorization1.description = 'Can submit mission';
    expectedAuthorizations.push(authorization1);

    service.authorizations = expectedAuthorizations;

    expect(service.authorization(authorization0.name)).toBeTrue();
    expect(service.authorization(authorization1.name)).toBeTrue();
    expect(service.authorization('whatever')).toBeFalse();
    expect(service.authorization([authorization0.name, authorization1.name])).toBeTrue();
    expect(service.authorization([authorization0.name, 'whatever'])).toBeFalse();
  });

  it('Authorization asynchronous', () => {
    expect(service.authorizations).toEqual([]);

    expect(service.authorization('whatever')).toBeFalse();
    expect(service.authorization(['whatever1', 'whatever2'])).toBeFalse();

    const expectedAuthorizations = [];

    const authorization0 = new AuthorizationResponse;
    authorization0.name = 'CanSeeHisSubmittedMissions';
    authorization0.description = 'Can consult the missions he submitted';
    expectedAuthorizations.push(authorization0);

    const authorization1 = new AuthorizationResponse;
    authorization1.name = 'CanSubmitMission';
    authorization1.description = 'Can submit mission';
    expectedAuthorizations.push(authorization1);

    const authorizationSubject = new Subject<AuthorizationResponse[]>();
    service.authorizationsRequestResponse = authorizationSubject;

    const authorizationResponse0 = service.authorization(authorization0.name);
    const authorizationResponse1 = service.authorization(authorization1.name);
    const authorizationResponseWhatever = service.authorization('whatever');

    expect(authorizationResponse0).toBeInstanceOf(Observable);
    expect(authorizationResponse1).toBeInstanceOf(Observable);
    expect(authorizationResponseWhatever).toBeInstanceOf(Observable);

    (authorizationResponse0 as Observable<boolean>).subscribe((result : boolean) => {
      expect(result).toBeTrue();
    });
    (authorizationResponse1 as Observable<boolean>).subscribe((result : boolean) => {
      expect(result).toBeTrue();
    });
    (authorizationResponseWhatever as Observable<boolean>).subscribe((result : boolean) => {
      expect(result).toBeFalse();
    });

    const authorizationArrayResponse0 = service.authorization([authorization0.name, authorization1.name]);
    const authorizationArrayResponse1 = service.authorization([authorization0.name, 'whatever']);

    expect(authorizationArrayResponse0).toBeInstanceOf(Observable);
    expect(authorizationArrayResponse1).toBeInstanceOf(Observable);

    (authorizationArrayResponse0 as Observable<boolean>).subscribe((result : boolean) => {
      expect(result).toBeTrue();
    });
    (authorizationArrayResponse1 as Observable<boolean>).subscribe((result : boolean) => {
      expect(result).toBeFalse();
    });

    authorizationSubject.next(expectedAuthorizations);
  });

  it('logout', () => {
    service.authenticationToken = 'aToken';
    service.authorizations = [new AuthorizationResponse];
    service.authorizationsRequestResponse = new Observable<AuthorizationResponse[]>();
    service.profiles = [new ProfileResponse];
    service.selectedProfile = new ProfileResponse;

    service.logout();

    expect(service.authenticationToken).toBeNull();
    expect(service.authorizations).toEqual([]);
    expect(service.authorizationsRequestResponse).toBeUndefined();
    expect(service.profiles).toEqual([]);
    expect(service.selectedProfile).toBeUndefined();
  });
});
