import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NonAuthenticatedGuard} from './guard/non-authenticated.guard';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {Authorizations} from 'common/model/authorizations';
import {AuthorizedGuard} from './guard/authorized.guard';

const routes: Routes = [
  {
    path: 'hero',
    canActivate: [AuthorizedGuard],
    data: {authorizations: [Authorizations.CanSeeHisSubmittedMissions, Authorizations.CanSubmitMission]},
    loadChildren: () => import('./hero/hero.module').then((m) => m.HeroModule),
  },
  {
    path: 'admin',
    canActivate: [AuthorizedGuard],
    data: {authorizations: [Authorizations.CanAdministrateUsers]},
    loadChildren: () => import('./admin/admin.module').then((m) => m.HeroModule),
  },
  {
    path: '',
    canActivate: [NonAuthenticatedGuard],
    loadChildren: () => import('./public/public.module').then((m) => m.PublicModule),
  },
  {path: '**', component: PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
