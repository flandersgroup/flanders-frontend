import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthorizedGuard} from '../guard/authorized.guard';
import {Authorizations} from 'common/model/authorizations';
import {AdminUsersComponent} from './users/users';

const routes: Routes = [
  {
    path: 'users', component: AdminUsersComponent,
    canActivate: [AuthorizedGuard],
    data: {authorizations: [Authorizations.CanAdministrateUsers]},
  },
  {path: '', redirectTo: 'users', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }
