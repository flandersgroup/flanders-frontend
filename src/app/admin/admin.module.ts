import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminRoutingModule} from './admin-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AdminUsersComponent} from './users/users';

@NgModule({
  declarations: [AdminUsersComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    NgbModule,
  ],
})
export class HeroModule { }
