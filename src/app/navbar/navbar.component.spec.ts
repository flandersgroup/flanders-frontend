import {ComponentFixture, TestBed} from '@angular/core/testing';
import {Router} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BehaviorSubject} from 'rxjs';
import {Authorizations} from 'common/model/authorizations';
import {AuthorizationService} from '../service/authorization.service';
import {HomeResolverService} from '../service/home-resolver.service';

import {NavbarComponent} from './navbar.component';
import {AuthenticationService} from '../service/authentication.service';
import {ProfileResponse} from 'common/server/response/profile';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let authorizationServiceSpy: jasmine.SpyObj<AuthorizationService>;
  let authenticationServiceSpy: jasmine.SpyObj<AuthenticationService>;
  let profilesSpyGetter: jasmine.Spy<jasmine.Func>;
  let selectedProfileSpyGetter: jasmine.Spy<jasmine.Func>;
  let routerSpy: jasmine.SpyObj<Router>;
  const homeResolverSpy = jasmine.createSpyObj<HomeResolverService>('HomeResolverService', ['userHome']);
  const mockUserHome = 'mockUserHome';

  beforeEach(async () => {
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    authorizationServiceSpy = jasmine.createSpyObj<AuthorizationService>('AuthorizationService',
        ['isAuthenticated', 'authorization'], {profiles: [], selectedProfile: undefined});
    authenticationServiceSpy = jasmine.createSpyObj<AuthenticationService>('AuthenticationService', ['login', 'logout', 'selectProfile']);

    profilesSpyGetter = Object.getOwnPropertyDescriptor(authorizationServiceSpy, 'profiles')?.get as jasmine.Spy<jasmine.Func>;
    profilesSpyGetter.and.returnValue([]);

    selectedProfileSpyGetter =
    Object.getOwnPropertyDescriptor(authorizationServiceSpy, 'selectedProfile')?.get as jasmine.Spy<jasmine.Func>;
    selectedProfileSpyGetter.and.returnValue(undefined);

    const testbed = TestBed.configureTestingModule({
      declarations: [NavbarComponent],
      providers: [
        {provide: AuthenticationService, useValue: authenticationServiceSpy},
        {provide: AuthorizationService, useValue: authorizationServiceSpy},
        {provide: HomeResolverService, useValue: homeResolverSpy},
        {
          provide: Router, useValue: routerSpy,
        }],
      imports: [
        NgbModule,
      ],
    });

    const userHome = new BehaviorSubject(mockUserHome);
    homeResolverSpy.userHome.and.returnValue(userHome);
    await testbed.compileComponents();
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('non authenticated', () => {
    it('Check displayed items', () => {
      authorizationServiceSpy.authorization.and.returnValue(false);
      authorizationServiceSpy.isAuthenticated.and.returnValue(false);
      expect(component.homeDisplayed).toBeTrue();
      expect(component.myMissionsDisplayed).toBeFalse();
      expect(component.submitMissionDisplayed).toBeFalse();
      expect(component.usersDisplayed).toBeFalse();
      expect(component.logoutDisplayed).toBeFalse();
      expect(component.registerDisplayed).toBeTrue();
      expect(component.logInDisplayed).toBeTrue();

      fixture.detectChanges();

      const compiled = fixture.nativeElement;
      const nodes = compiled.querySelector('#navbarButtonList');
      const listElements = nodes.getElementsByTagName('li');

      expect(listElements.length).toEqual(1);

      expect(listElements[0]).not.toBeNull();
      expect(listElements[0].firstChild.textContent).toEqual('Home');

      expect(compiled.querySelector('#navbarLogoutButton')).toBeNull();
      const registerButton = compiled.querySelector('#navbarRegisterButton');
      expect(registerButton).not.toBeNull();
      expect(registerButton.textContent).toEqual(' Register ');
      const loginButton = compiled.querySelector('#navbarLoginButton');
      expect(loginButton).not.toBeNull();
      expect(loginButton.textContent).toEqual(' Log In ');
    });

    it('Click on home icon', () => {
      authorizationServiceSpy.isAuthenticated.and.returnValue(false);
      const spyNavbar = spyOn(component, 'home').and.callThrough();
      fixture.detectChanges();

      const homeButton = fixture.nativeElement.querySelector('#navbarHomeButton');
      homeButton.click();

      expect(spyNavbar).toHaveBeenCalled();
      expect(routerSpy.navigate).toHaveBeenCalledOnceWith([mockUserHome]);
    });

    it('Click on navbar register button', () => {
      authorizationServiceSpy.isAuthenticated.and.returnValue(false);
      const spyNavbar = spyOn(component, 'register').and.callThrough();
      fixture.detectChanges();

      const compiled = fixture.nativeElement;
      const loginButton = compiled.querySelector('#navbarRegisterButton');
      expect(loginButton).not.toBeNull();

      loginButton.click();

      expect(spyNavbar).toHaveBeenCalled();
      expect(routerSpy.navigate).toHaveBeenCalledOnceWith(['register']);
    });

    it('Click on navbar login button', () => {
      authorizationServiceSpy.isAuthenticated.and.returnValue(false);
      const spyNavbar = spyOn(component, 'login').and.callThrough();
      fixture.detectChanges();

      const compiled = fixture.nativeElement;
      const loginButton = compiled.querySelector('#navbarLoginButton');
      expect(loginButton).not.toBeNull();

      loginButton.click();

      expect(spyNavbar).toHaveBeenCalled();
      expect(routerSpy.navigate).toHaveBeenCalledOnceWith(['login']);
    });
  });

  describe('authenticated without authorization', () => {
    it('Check displayed items', async () => {
      authorizationServiceSpy.authorization.and.returnValue(false);
      authorizationServiceSpy.isAuthenticated.and.returnValue(true);
      expect(component.homeDisplayed).toBeFalse();
      expect(component.myMissionsDisplayed).toBeFalse();
      expect(component.submitMissionDisplayed).toBeFalse();
      expect(component.usersDisplayed).toBeFalse();
      expect(component.logoutDisplayed).toBeTrue();
      expect(component.registerDisplayed).toBeFalse();
      expect(component.logInDisplayed).toBeFalse();

      fixture.detectChanges();
      await fixture.whenStable();

      const compiled = fixture.nativeElement;
      const nodes = compiled.querySelector('#navbarButtonList');
      const listElements = nodes.getElementsByTagName('li');

      expect(listElements.length).toEqual(0);

      const logoutButton = compiled.querySelector('#navbarLogoutButton');
      expect(logoutButton).not.toBeNull();
      expect(logoutButton.textContent).toEqual('Logout');
      const registerButton = compiled.querySelector('#navbarRegisterButton');
      expect(registerButton).toBeNull();
      const loginButton = compiled.querySelector('#navbarLoginButton');
      expect(loginButton).toBeNull();
    });

    it('Click on navbar logout button', () => {
      authorizationServiceSpy.isAuthenticated.and.returnValue(true);
      const spyNavbar = spyOn(component, 'logout').and.callThrough();
      fixture.detectChanges();

      const compiled = fixture.nativeElement;
      const loginButton = compiled.querySelector('#navbarLogoutButton');
      expect(loginButton).not.toBeNull();

      loginButton.click();

      expect(spyNavbar).toHaveBeenCalled();
      expect(authenticationServiceSpy.logout).toHaveBeenCalled();
      expect(routerSpy.navigate).toHaveBeenCalledOnceWith([mockUserHome]);
    });
  });

  it('CanSubmitMission', () => {
    authorizationServiceSpy.authorization.and.returnValue(false);
    authorizationServiceSpy.authorization.withArgs(Authorizations.CanSubmitMission).and.returnValue(true);
    authorizationServiceSpy.isAuthenticated.and.returnValue(true);
    expect(component.homeDisplayed).toBeFalse();
    expect(component.myMissionsDisplayed).toBeFalse();
    expect(component.submitMissionDisplayed).toBeTrue();
    expect(component.usersDisplayed).toBeFalse();
    expect(component.logoutDisplayed).toBeTrue();
    expect(component.registerDisplayed).toBeFalse();
    expect(component.logInDisplayed).toBeFalse();

    fixture.detectChanges();

    const compiled = fixture.nativeElement;
    const nodes = compiled.querySelector('#navbarButtonList');
    const listElements = nodes.getElementsByTagName('li');

    expect(listElements.length).toEqual(1);

    expect(listElements[0]).not.toBeNull();
    expect(listElements[0].firstChild.textContent).toEqual('Submit mission');
  });

  it('CanSeeHisSubmittedMissions', () => {
    authorizationServiceSpy.authorization.and.returnValue(false);
    authorizationServiceSpy.authorization.withArgs(Authorizations.CanSeeHisSubmittedMissions).and.returnValue(true);
    authorizationServiceSpy.isAuthenticated.and.returnValue(true);
    expect(component.homeDisplayed).toBeFalse();
    expect(component.myMissionsDisplayed).toBeTrue();
    expect(component.submitMissionDisplayed).toBeFalse();
    expect(component.usersDisplayed).toBeFalse();
    expect(component.logoutDisplayed).toBeTrue();
    expect(component.registerDisplayed).toBeFalse();
    expect(component.logInDisplayed).toBeFalse();

    fixture.detectChanges();

    const compiled = fixture.nativeElement;
    const nodes = compiled.querySelector('#navbarButtonList');
    const listElements = nodes.getElementsByTagName('li');

    expect(listElements.length).toEqual(1);

    expect(listElements[0]).not.toBeNull();
    expect(listElements[0].firstChild.textContent).toEqual('My missions');
  });

  it('CanSubmitMission and CanSeeHisSubmittedMissions', () => {
    authorizationServiceSpy.authorization.and.returnValue(false);
    authorizationServiceSpy.authorization.withArgs(Authorizations.CanSeeHisSubmittedMissions).and.returnValue(true);
    authorizationServiceSpy.authorization.withArgs(Authorizations.CanSubmitMission).and.returnValue(true);
    authorizationServiceSpy.isAuthenticated.and.returnValue(true);
    expect(component.homeDisplayed).toBeFalse();
    expect(component.myMissionsDisplayed).toBeTrue();
    expect(component.submitMissionDisplayed).toBeTrue();
    expect(component.usersDisplayed).toBeFalse();
    expect(component.logoutDisplayed).toBeTrue();
    expect(component.registerDisplayed).toBeFalse();
    expect(component.logInDisplayed).toBeFalse();

    fixture.detectChanges();

    const compiled = fixture.nativeElement;
    const nodes = compiled.querySelector('#navbarButtonList');
    const listElements = nodes.getElementsByTagName('li');

    expect(listElements.length).toEqual(2);

    expect(listElements[0]).not.toBeNull();
    expect(listElements[0].firstChild.textContent).toEqual('My missions');

    expect(listElements[1]).not.toBeNull();
    expect(listElements[1].firstChild.textContent).toEqual('Submit mission');
  });

  it('CanAdministrateUsers', () => {
    authorizationServiceSpy.authorization.and.returnValue(false);
    authorizationServiceSpy.authorization.withArgs(Authorizations.CanAdministrateUsers).and.returnValue(true);
    authorizationServiceSpy.isAuthenticated.and.returnValue(true);
    expect(component.homeDisplayed).toBeFalse();
    expect(component.myMissionsDisplayed).toBeFalse();
    expect(component.submitMissionDisplayed).toBeFalse();
    expect(component.usersDisplayed).toBeTrue();
    expect(component.logoutDisplayed).toBeTrue();
    expect(component.registerDisplayed).toBeFalse();
    expect(component.logInDisplayed).toBeFalse();

    fixture.detectChanges();

    const compiled = fixture.nativeElement;
    const nodes = compiled.querySelector('#navbarButtonList');
    const listElements = nodes.getElementsByTagName('li');

    expect(listElements.length).toEqual(1);

    expect(listElements[0]).not.toBeNull();
    expect(listElements[0].firstChild.textContent).toEqual('Administrate users');
  });

  describe('selectProfile', () => {
    it('noProfile', () => {
      const compiled = fixture.nativeElement;
      const dropDownProfiles = compiled.querySelector('#dropdownProfiles');
      expect(dropDownProfiles).toBeNull();
    });
    it('one profile', () => {
      const vilainProfile = new ProfileResponse;
      vilainProfile.name = 'Vilain';
      vilainProfile.description = 'A vilain who can edit his profile and receive missions';

      profilesSpyGetter.and.returnValue([vilainProfile]);

      fixture.detectChanges();

      const compiled = fixture.nativeElement;
      const dropDownProfiles = compiled.querySelector('#dropdownProfiles');
      expect(dropDownProfiles).toBeNull();
    });
    it('multiple profile', () => {
      const profilesResponse: ProfileResponse[] = [];

      const adminProfile = new ProfileResponse;
      adminProfile.name = 'Administrator';
      adminProfile.description = 'Administrator of the website, able to administrate users';
      profilesResponse.push(adminProfile);

      const vilainProfile = new ProfileResponse;
      vilainProfile.name = 'Vilain';
      vilainProfile.description = 'A vilain who can edit his profile and receive missions';
      profilesResponse.push(vilainProfile);

      profilesSpyGetter.and.returnValue(profilesResponse);
      selectedProfileSpyGetter.and.returnValue(adminProfile);

      fixture.detectChanges();

      const compiled = fixture.nativeElement;
      const dropDownProfiles = compiled.querySelector('#dropdownProfiles');
      expect(dropDownProfiles).not.toBeNull();

      const dropdownProfilesButton = compiled.querySelector('#dropdownProfilesButton');
      expect(dropdownProfilesButton).not.toBeNull();
      expect(dropdownProfilesButton.textContent).toEqual(adminProfile.name);
      dropdownProfilesButton.click();

      const dropdownProfilesElement = compiled.querySelector('#dropdownProfilesElement');
      expect(dropdownProfilesElement).not.toBeNull();
      expect(dropdownProfilesElement.childElementCount).toEqual(2);
      const childrens = dropdownProfilesElement.children;
      expect(childrens[0].textContent).toEqual(adminProfile.name);
      expect(childrens[1].textContent).toEqual(vilainProfile.name);
      childrens[1].click();

      fixture.detectChanges();

      expect(authenticationServiceSpy.selectProfile).toHaveBeenCalledOnceWith(vilainProfile);
    });
  });
});
