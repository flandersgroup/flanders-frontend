import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Authorizations} from 'common/model/authorizations';
import {AuthorizationService} from '../service/authorization.service';
import {HomeResolverService} from '../service/home-resolver.service';
import {ProfileResponse} from 'common/server/response/profile';
import {AuthenticationService} from '../service/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  isNavbarCollapsed = true;

  constructor(
    private _authorizationService: AuthorizationService,
    private _router: Router,
    private _authenticationService: AuthenticationService,
    private _homeResolver: HomeResolverService) { }

  ngOnInit(): void { }

  home(): void {
    this._homeResolver.userHome().subscribe((home) => this._router.navigate([home]));
  }

  register(): void {
    this._router.navigate(['register']);
  }

  login(): void {
    this._router.navigate(['login']);
  }

  logout() {
    this._authenticationService.logout();
    this._homeResolver.userHome().subscribe((home) => this._router.navigate([home]));
  }

  get profiles(): ProfileResponse[] {
    return this._authorizationService.profiles;
  }

  get selectedProfile(): ProfileResponse | undefined {
    return this._authorizationService.selectedProfile;
  }

  selectProfile(profile: ProfileResponse) {
    this._authenticationService.selectProfile(profile);
  }

  get homeDisplayed(): boolean {
    return !this._authorizationService.isAuthenticated();
  }

  get submitMissionDisplayed(): boolean {
    return this._authorization(Authorizations.CanSubmitMission);
  }

  get myMissionsDisplayed(): boolean {
    return this._authorization(Authorizations.CanSeeHisSubmittedMissions);
  }

  get usersDisplayed(): boolean {
    return this._authorization(Authorizations.CanAdministrateUsers);
  }

  get logoutDisplayed(): boolean {
    return this._authorizationService.isAuthenticated();
  }

  get registerDisplayed(): boolean {
    return !this._authorizationService.isAuthenticated();
  }

  get logInDisplayed(): boolean {
    return !this._authorizationService.isAuthenticated();
  }

  private _authorization(authorization: string): boolean {
    const resultAuthorization = this._authorizationService.authorization(authorization);

    if (resultAuthorization instanceof Observable) {
      return false;
    } else {
      return resultAuthorization;
    }
  }
}
