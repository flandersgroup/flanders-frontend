import {TestBed} from '@angular/core/testing';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {AuthorizationService} from '../service/authorization.service';
import {HomeResolverService} from '../service/home-resolver.service';

import {AuthorizedGuard} from './authorized.guard';

describe('AuthorizedGuard', () => {
  let guard: AuthorizedGuard;
  let authorizationServiceSpy: jasmine.SpyObj<AuthorizationService>;
  let routerSpy: jasmine.SpyObj<Router>;
  let homeResolverSpy: jasmine.SpyObj<HomeResolverService>;

  beforeEach(() => {
    authorizationServiceSpy = jasmine.createSpyObj<AuthorizationService>('AuthorizationService', ['authorization']);
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    homeResolverSpy = jasmine.createSpyObj<HomeResolverService>('HomeResolverService', ['userHome']);

    TestBed.configureTestingModule({
      providers: [
        AuthorizedGuard,
        {provide: AuthorizationService, useValue: authorizationServiceSpy},
        {provide: HomeResolverService, useValue: homeResolverSpy},
        {provide: Router, useValue: routerSpy},
      ],
    });
    guard = TestBed.inject(AuthorizedGuard);
    const userHome = new BehaviorSubject('Whatever');
    homeResolverSpy.userHome.and.returnValue(userHome);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('non authorized', () => {
    const route = new ActivatedRouteSnapshot;
    route.data = {authorizations: ['a', 'b']};
    const state = {url: 'afakeUrl '} as RouterStateSnapshot;

    authorizationServiceSpy.authorization.and.returnValue(false);

    expect(guard.canActivate(route, state)).toBeFalse();
    expect(authorizationServiceSpy.authorization).toHaveBeenCalledOnceWith(route.data.authorizations);
    expect(routerSpy.navigate).toHaveBeenCalledOnceWith(['Whatever']);
  });

  it('authorized', () => {
    const route = new ActivatedRouteSnapshot;
    route.data = {authorizations: ['b', 'c']};
    const state = {url: 'afakeUrl '} as RouterStateSnapshot;

    authorizationServiceSpy.authorization.and.returnValue(true);

    expect(guard.canActivate(route, state)).toBeTrue();
    expect(authorizationServiceSpy.authorization).toHaveBeenCalledOnceWith(route.data.authorizations);
    expect(routerSpy.navigate).toHaveBeenCalledTimes(0);
  });

  it('non authorized Observable', (done) => {
    const route = new ActivatedRouteSnapshot;
    route.data = {authorizations: ['a', 'b']};
    const state = {url: 'afakeUrl '} as RouterStateSnapshot;

    authorizationServiceSpy.authorization.and.returnValue(of(false));

    const returnValue = guard.canActivate(route, state) as Observable<Boolean>;
    returnValue.subscribe((result) => {
      expect(result).toBeFalse();
      expect(authorizationServiceSpy.authorization).toHaveBeenCalledOnceWith(route.data.authorizations);
      expect(routerSpy.navigate).toHaveBeenCalledOnceWith(['Whatever']);
      done();
    }, fail);
  });

  it('authorized Observable', (done) => {
    const route = new ActivatedRouteSnapshot;
    route.data = {authorizations: ['b', 'c']};
    const state = {url: 'afakeUrl'} as RouterStateSnapshot;

    authorizationServiceSpy.authorization.and.returnValue(of(true));

    const returnValue = guard.canActivate(route, state) as Observable<Boolean>;
    returnValue.subscribe((result) => {
      expect(result).toBeTrue();
      expect(authorizationServiceSpy.authorization).toHaveBeenCalledOnceWith(route.data.authorizations);
      expect(routerSpy.navigate).toHaveBeenCalledOnceWith(['afakeUrl']);
      done();
    }, fail);
  });
});
