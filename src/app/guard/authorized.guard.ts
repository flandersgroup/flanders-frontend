import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthorizationService} from '../service/authorization.service';
import {HomeResolverService} from '../service/home-resolver.service';

@Injectable({
  providedIn: 'root',
})

export class AuthorizedGuard implements CanActivate {
  constructor(
    private authorizationService: AuthorizationService,
    private router: Router,
    private homeResolver: HomeResolverService,
  ) { }

  canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): boolean | Observable<boolean> {
    const authorizations = route.data.authorizations as Array<string>;
    const authorizationObservable = this.authorizationService.authorization(authorizations);

    if (authorizationObservable instanceof Observable) {
      authorizationObservable.subscribe((result) => {
        if (!result) {
          this.homeResolver.userHome().subscribe((home) => this.router.navigate([home]));
        } else {
          this.router.navigate([state.url]);
        }
      });
    } else {
      if (!authorizationObservable) {
        this.homeResolver.userHome().subscribe((home) => this.router.navigate([home]));
      }
    }

    return authorizationObservable;
  }
}
