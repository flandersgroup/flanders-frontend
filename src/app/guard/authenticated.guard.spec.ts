import {TestBed} from '@angular/core/testing';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import {AuthenticatedGuard} from './authenticated.guard';
import {HomeResolverService} from '../service/home-resolver.service';
import {RouterTestingModule} from '@angular/router/testing';
import {BehaviorSubject} from 'rxjs';
import {AuthorizationService} from '../service/authorization.service';

describe('AuthenticatedGuard', () => {
  let guard: AuthenticatedGuard;
  let routerSpy: jasmine.SpyObj<Router>;
  let homeResolverSpy: jasmine.SpyObj<HomeResolverService>;
  let authorizationServiceSpy : jasmine.SpyObj<AuthorizationService>;

  beforeEach(() => {
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    homeResolverSpy = jasmine.createSpyObj<HomeResolverService>('HomeResolverService', ['userHome']);
    authorizationServiceSpy = jasmine.createSpyObj<AuthorizationService>('AuthorizationService', ['isAuthenticated']);

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      providers: [
        AuthenticatedGuard,
        {provide: HomeResolverService, useValue: homeResolverSpy},
        {provide: Router, useValue: routerSpy},
        {provide: AuthorizationService, useValue: authorizationServiceSpy},
      ],
    });
    guard = TestBed.inject(AuthenticatedGuard);
    const userHome = new BehaviorSubject('Whatever');
    homeResolverSpy.userHome.and.returnValue(userHome);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('authenticated', () => {
    authorizationServiceSpy.isAuthenticated.and.returnValue(true);
    expect(guard.canActivate(new ActivatedRouteSnapshot(), {url: 'afakeUrl '} as RouterStateSnapshot)).toBeTrue();
    expect(routerSpy.navigate).toHaveBeenCalledTimes(0);
  });

  it('Non authenticated', () => {
    authorizationServiceSpy.isAuthenticated.and.returnValue(false);
    expect(guard.canActivate(new ActivatedRouteSnapshot(), {url: 'afakeUrl '} as RouterStateSnapshot)).toBeFalse();
    expect(routerSpy.navigate).toHaveBeenCalledOnceWith(['Whatever']);
  });
});
