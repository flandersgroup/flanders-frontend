import {TestBed} from '@angular/core/testing';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {BehaviorSubject} from 'rxjs';
import {AuthorizationService} from '../service/authorization.service';
import {HomeResolverService} from '../service/home-resolver.service';
import {NonAuthenticatedGuard} from './non-authenticated.guard';

describe('NonAuthenticatedGuard', () => {
  let guard: NonAuthenticatedGuard;
  let routerSpy: jasmine.SpyObj<Router>;
  let homeResolver: jasmine.SpyObj<HomeResolverService>;
  let authorizationServiceSpy : jasmine.SpyObj<AuthorizationService>;

  beforeEach(() => {
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    homeResolver = jasmine.createSpyObj<HomeResolverService>('HomeResolverService', ['userHome']);
    authorizationServiceSpy = jasmine.createSpyObj<AuthorizationService>('AuthorizationService', ['isAuthenticated']);

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule],
      providers: [
        NonAuthenticatedGuard,
        {provide: HomeResolverService, useValue: homeResolver},
        {provide: Router, useValue: routerSpy},
        {provide: AuthorizationService, useValue: authorizationServiceSpy},
      ],
    });
    guard = TestBed.inject(NonAuthenticatedGuard);
    const userHome = new BehaviorSubject('Whatever');
    homeResolver.userHome.and.returnValue(userHome);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('authenticated', () => {
    authorizationServiceSpy.isAuthenticated.and.returnValue(true);
    expect(guard.canActivate(new ActivatedRouteSnapshot(), {url: 'afakeUrl '} as RouterStateSnapshot)).toBeFalse();
    expect(routerSpy.navigate).toHaveBeenCalledOnceWith(['Whatever']);
  });

  it('Non authenticated', () => {
    authorizationServiceSpy.isAuthenticated.and.returnValue(false);
    expect(guard.canActivate(new ActivatedRouteSnapshot(), {url: 'afakeUrl '} as RouterStateSnapshot)).toBeTrue();
    expect(routerSpy.navigate).toHaveBeenCalledTimes(0);
  });
});
