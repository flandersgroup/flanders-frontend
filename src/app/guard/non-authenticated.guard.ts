import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {AuthorizationService} from '../service/authorization.service';
import {HomeResolverService} from '../service/home-resolver.service';

@Injectable({
  providedIn: 'root',
})
export class NonAuthenticatedGuard implements CanActivate {
  constructor(
    private authorizationService : AuthorizationService,
    private router: Router,
    private homeResolver: HomeResolverService,
  ) { }

  canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): boolean {
    if (this.authorizationService.isAuthenticated()) {
      this.homeResolver.userHome().subscribe((home) => this.router.navigate([home]));
    }
    return !this.authorizationService.isAuthenticated();
  }
}
